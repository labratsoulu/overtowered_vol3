﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class CameraText : MonoBehaviour {

    private GUIStyle style;

    void Awake () {
        	
	}

    void OnDrawGizmos()
    {
        style = new GUIStyle();
        style.fontSize = 20;
        //Handles.color = Color.black;

        #if UNITY_EDITOR
        Handles.Label(transform.position + new Vector3(0,20,0), gameObject.name, style);
        #endif
    }
}
