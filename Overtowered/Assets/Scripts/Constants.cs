﻿using UnityEngine;
using System.Collections;

public class Constants : MonoBehaviour {

    [Header("Game Settings")]

    [Tooltip("Build version. Only players with same version can join the same game.")]
    public string versionNumber;


    [Header("Player Variables")]

    [Tooltip("Number of players required to start the game. Set this to 1 to test single player, 2 for multiplayer")]
    public int playersToStart;

    [Tooltip("Maximum number of players in a room")]
    public byte maxPlayers;

    [Tooltip("Starting health of the towers")]
    public float towerStartHealth;

    [Tooltip("Amount of magic at the start (per player)")]
    public float magicStartAmount;

    [Tooltip("Maximum amount of mana (per magic bar)")]
    public float maxMagic;

    [Tooltip("Amount of magic each tower piece costs")]
    public float pieceMagicCost;

    [Tooltip("Amount of Arcane Bombs per game")]
    public float arcaneBombAmount;

    [Tooltip("Cooldown after spending magic after which magic starts recharging again")]
    public float rechargeCooldown;

    [Tooltip("Amount of magic recovered every step when recharging (magic increases by *rate* every *step* seconds)")]
    public float magicRechargeRate;

    [Tooltip("The interval of magic recharge (magic increases by *rate* every *step* seconds)")]
    public float magicRechargeStep;

    [Tooltip("The maximum number of units one player can have at any given time)")]
    public float maxUnitAmount;

    [Tooltip("Whether or not killing units gives magic to the killer")]
    public bool giveMagicOnKill;

    [Tooltip("Offline mode on/off for testing")]
    public bool offlineMode;

    [Tooltip("Test mode on/off, disables unit limits")]
    public bool testMode;

    [Tooltip("Lock camera X axis on/off")]
    public bool cameraXLock;

    [Tooltip("If X lock is used, rotation of X axis in degrees")]
    public float cameraXRotation;


    [Header("Unit Specific Variables")]

    [Tooltip("Starting and max health of the unit")]
    public float golemHealth;

    [Tooltip("Damage of the unit")]
    public float golemDamage;

    [Tooltip("Attack speed of the unit (Unit attacks every *interval* seconds)")]
    public float golemAttackInterval;

    [Tooltip("Movement speed of the unit")]
    public float golemMovementSpeed;

    [Tooltip("Attack distance of the unit")]
    public float golemAttackDistance;

    [Tooltip("Amount of magic the other player is given when killing this unit")]
    public float golemMagicReward;

	[Tooltip("The time it takes for 1 golem to spawn")]
	public float golemSpawnTime;

    [Tooltip("The pool point and tower damage value of the unit")]
    public float golemValue;

    [Space(5)]

    [Tooltip("Starting and max health of the unit")]
    public float apprenticeHealth;

    [Tooltip("Damage of the unit")]
    public float apprenticeDamage;

    [Tooltip("Attack speed of the unit (Unit attacks every *interval* seconds)")]
    public float apprenticeAttackInterval;

    [Tooltip("Movement speed of the unit")]
    public float apprenticeMovementSpeed;

    [Tooltip("Attack distance of the unit")]
    public float apprenticeAttackDistance;

    [Tooltip("Amount of magic the other player is given when killing this unit")]
    public float apprenticeMagicReward;

	[Tooltip("The time it takes for 1 apprentice to spawn")]
	public float apprenticeSpawnTime;

    [Tooltip("The pool point and tower damage value of the unit")]
    public float apprenticeValue;

    [Space(5)]

    [Tooltip("Starting and max health of the unit")]
    public float skeletonHealth;

    [Tooltip("Damage of the unit")]
    public float skeletonDamage;

    [Tooltip("Attack speed of the unit (Unit attacks every *interval* seconds)")]
    public float skeletonAttackInterval;

    [Tooltip("Movement speed of the unit")]
    public float skeletonMovementSpeed;

    [Tooltip("Attack distance of the unit")]
    public float skeletonAttackDistance;

    [Tooltip("Amount of magic the other player is given when killing this unit")]
    public float skeletonMagicReward;

	[Tooltip("The time it takes for 1 skeleton to spawn")]
	public float skeletonSpawnTime;

    [Tooltip("The pool point and tower damage value of the unit")]
    public float skeletonValue;

    [Space(5)]

    [Tooltip("Starting and max health of the unit")]
    public float emmettHealth;

    [Tooltip("Damage of the unit")]
    public float emmettDamage;

    [Tooltip("Attack speed of the unit (Unit attacks every *interval* seconds)")]
    public float emmettAttackInterval;

    [Tooltip("Movement speed of the unit")]
    public float emmettMovementSpeed;

    [Tooltip("Attack distance of the unit")]
    public float emmettAttackDistance;

    [Tooltip("Amount of magic the other player is given when killing this unit")]
    public float emmettMagicReward;

    [Tooltip("The time it takes for 1 Emmett to spawn")]
    public float emmettSpawnTime;

    [Tooltip("The pool point and tower damage value of the unit")]
    public float emmettValue;

    [Space(5)]

    [Tooltip("Starting and max health of the unit")]
    public float sarkoHealth;

    [Tooltip("Damage of the unit")]
    public float sarkoDamage;

    [Tooltip("Attack speed of the unit (Unit attacks every *interval* seconds)")]
    public float sarkoAttackInterval;

    [Tooltip("Movement speed of the unit")]
    public float sarkoMovementSpeed;

    [Tooltip("Attack distance of the unit")]
    public float sarkoAttackDistance;

    [Tooltip("Amount of magic the other player is given when killing this unit")]
    public float sarkoMagicReward;

    [Tooltip("The time it takes for 1 Sarko to spawn")]
    public float sarkoSpawnTime;

    [Tooltip("The pool point and tower damage value of the unit")]
    public float sarkoValue;

    [Space(5)]

    [Tooltip("Starting and max health of the unit")]
    public float lamielHealth;

    [Tooltip("Damage of the unit")]
    public float lamielDamage;

    [Tooltip("Attack speed of the unit (Unit attacks every *interval* seconds)")]
    public float lamielAttackInterval;

    [Tooltip("Movement speed of the unit")]
    public float lamielMovementSpeed;

    [Tooltip("Attack distance of the unit")]
    public float lamielAttackDistance;

    [Tooltip("Amount of magic the other player is given when killing this unit")]
    public float lamielMagicReward;

    [Tooltip("The time it takes for 1 Lamiel to spawn")]
    public float lamielSpawnTime;

    [Tooltip("The pool point and tower damage value of the unit")]
    public float lamielValue;


    [Header("Global Unit Variables")]

    [Tooltip("How often the unit checks for targets around it (every *interval* seconds). Dont't set this too low.")]
    public float targetCheckInterval; // 0.5

    [Tooltip("How far the unit acquires targets")]
    public float targetAcquireDistance; //30

    [Tooltip("Unit spawns every *rate* seconds")]
    public float unitSpawnRate; // 10

    [Tooltip("Unit spawn area radius")]
    public float unitSpawnRadius;

    [Tooltip("How long the corpse lasts until it is destroyed. Affects the timing of the healing spell resurrection.")]
    public float deathTime;

    [Tooltip("Unit explosion radius if unit is set ot explode")]
    public float unitExplosionRadius;

    [Space(5)]

    [Header("Spell Variables")]

	[Tooltip("Cost to cast the boost spell")]
	public float boostSpellCost = 30;

	[Tooltip("The cooldown of the boost spell in seconds")]
	public float boostSpellCD = 20;

	[Tooltip("The number to multiply the original stats with when casting the boost spell")]
	public float boostSpellBoost = 1.25f;

	[Tooltip("Duration of the boost spell")]
	public float boostSpellDuration = 30;

	[Tooltip("Cost to cast the damage spell")]
	public float damageSpellCost = 30;

	[Tooltip("The cooldown of the damage spell in seconds")]
	public float damageSpellCD = 20;

	[Tooltip("the amount of damage the damage spell does")]
	public float damageSpellDamage = 50;

    [Tooltip("How long the damage spell stuns the targets (used to allow some air time before collision check kicks in)")]
    public float damageSpellStunDuration = 1f;

    [Tooltip("Cost to cast the heal spell")]
	public float healSpellCost = 25;

	[Tooltip("The cooldown of the heal spell in seconds")]
	public float healSpellCD = 20;

	[Tooltip("the amount of healing the heal spell does, as a percentage of max health")]
	public float healSpellHealing = 0.25f;

	[Tooltip("Buff duration")]
	public float buffDuration = 15;

	[Tooltip("Spell Radius")]
	public float spellRadius = 25;

    [Space(5)]

    [Header("Tower Variables")]

	[Tooltip("Golem piece price")]
	public float golemPrice = 25;

	[Tooltip("Skeleton piece price")]
	public float skeletonPrice = 25;

	[Tooltip("Cultist piece price")]
	public float CultistPrice = 25;

    [Tooltip("Emmett piece price")]
    public float emmettPrice = 25;

    [Tooltip("Sarko piece price")]
    public float sarkoPrice = 25;

    [Tooltip("Lamiel piece price")]
    public float lamielPrice = 25;

    [Tooltip("Boost spell piece price")]
	public float boostPiecePrice = 60;

	[Tooltip("Damage spell piece price")]
	public float damagePiecePrice = 60;

	[Tooltip("Heal spell piece price")]
	public float healPiecePrice = 50;

	[Tooltip("Max magic piece price")]
	public float maxMagicPiecePrice = 70;

	[Tooltip("Magic regen piece price")]
	public float magicRegenPiecePrice = 50;

	[Tooltip("Exploding units piece price")]
	public float explodingUnitsPiecePrice = 50;

	[Tooltip("Boost spell piece adjacency bonus")]
	public float boostPieceBonus = 0.25f;

	[Tooltip("Damage spell piece adjacency bonus")]
	public float damagePieceBonus = 0.20f;

	[Tooltip("Heal spell piece adjacency bonus")]
	public float healPieceBonus = 0.20f;

	[Tooltip("Max magic upgrade piece adjacency bonus")]
	public float maxMagicPieceBonus = 0.10f;
}
