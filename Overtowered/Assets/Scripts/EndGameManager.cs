﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using UnityEngine.SceneManagement;

public class EndGameManager : MonoBehaviour {

    public GameObject endGameWindow;
    public Text player1Name;
    public Text player2Name;
    public Text player1ScoreText;
    public Text player2ScoreText;
    public Image player1Image;
    public Image player2Image;
    public Image outcomeImage;
    public Sprite[] outcomeImages;
    public Sprite[] wizardImages;
    public GameObject player1ReadyMark;
    public GameObject player2ReadyMark;
    public int player1Score;
    public int player2Score;

    private PhotonView thisView;

    void OnEnable()
    {
        thisView = GetComponent<PhotonView>();

        player1Score = (int)PhotonPlayer.Find(1).CustomProperties["score"];

        if (!PhotonNetwork.offlineMode)
        {
            player2Score = (int)PhotonPlayer.Find(2).CustomProperties["score"];
        }
    }

    //This function is called in an RPC, meaning that both players run this function at the same time.
    public void SetupEndGameWindow(int loserID)
    {
        //This is to let both players know the score
        if (loserID == 1)
        {
            player2Score++;
        }

        else // ID == 2
        { 
            player1Score++;
        }

        // This is to set player specific stuff in the endgame window, namely the outcome image.
        if (loserID == PhotonNetwork.player.ID) // We are the loser
        {
            if (PhotonNetwork.player.ID == 1)
            {
                // Red Turby Defeat
                if ((int)PhotonNetwork.player.CustomProperties["selectedWizard"] == 0)
                {
                    outcomeImage.sprite = outcomeImages[0];
                }

                // Red Tak Defeat
                else // == 1
                {
                    outcomeImage.sprite = outcomeImages[1];
                }
            }

            else // ID == 2
            {
                // Blue Turby Defeat
                if ((int)PhotonNetwork.player.CustomProperties["selectedWizard"] == 0)
                {
                    outcomeImage.sprite = outcomeImages[2];
                }

                // Blue Tak Defeat
                else // == 1
                {
                    outcomeImage.sprite = outcomeImages[3];
                }
            }
            
        }

        else // We are the winner
        {
            int tempScore = (int)PhotonNetwork.player.CustomProperties["score"];
            tempScore++;
            PhotonNetwork.player.SetCustomProperties(new Hashtable() { { "score", tempScore } });

            if (PhotonNetwork.player.ID == 1)
            {
                // Red Turby Victory
                if ((int)PhotonNetwork.player.CustomProperties["selectedWizard"] == 0)
                {
                    outcomeImage.sprite = outcomeImages[4];
                }

                // Red Tak Victory
                else // == 1
                {
                    outcomeImage.sprite = outcomeImages[5];
                }
            }

            else // ID == 2
            {
                // Blue Turby Victory
                if ((int)PhotonNetwork.player.CustomProperties["selectedWizard"] == 0)
                {
                    outcomeImage.sprite = outcomeImages[6];
                }

                // Blue Tak Victory
                else // == 1
                {
                    outcomeImage.sprite = outcomeImages[7];
                }
            }
        }

        player1Name.text = PhotonPlayer.Find(1).NickName;
        player1ScoreText.text = player1Score.ToString();
        player1Image.sprite = wizardImages[(int)PhotonPlayer.Find(1).CustomProperties["selectedWizard"]];
        player2Name.text = PhotonNetwork.offlineMode ? "" : PhotonPlayer.Find(2).NickName;
        player2ScoreText.text = PhotonNetwork.offlineMode ? "0" : player2Score.ToString();
        player2Image.sprite = PhotonNetwork.offlineMode ? wizardImages[1] : wizardImages[(int)PhotonPlayer.Find(2).CustomProperties["selectedWizard"]];

        endGameWindow.SetActive(true);
    }

    public void ToggleReady()
    {
        thisView.RPC("ToggleReadyMark", PhotonTargets.All, PhotonNetwork.player.ID);

        if ((player1ReadyMark.activeInHierarchy && player2ReadyMark.activeInHierarchy) || PhotonNetwork.offlineMode)
        {
            thisView.RPC("Rematch", PhotonTargets.All);
        }
    }

    [PunRPC]
    public void ToggleReadyMark(int playerID)
    {
        if (playerID == 1)
        {
            player1ReadyMark.SetActive(!player1ReadyMark.activeInHierarchy);
        }

        else // ID == 2
        {
            player2ReadyMark.SetActive(!player2ReadyMark.activeInHierarchy);
        }
    }

    [PunRPC]
    public void Rematch()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
