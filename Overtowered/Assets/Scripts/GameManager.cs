﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class GameManager : MonoBehaviour {

    public Camera mainCamera;
    public Transform[] cameraPositions;
    public Constants constants;
    public NetworkInGame networkInGame;
    public GameObject[] Canvases;
    public GameObject menu;
    public GameObject optionsMenu;
    public PlayerVars player1Vars;
    public PlayerVars player2Vars;
    public GameObject redBase;
    public GameObject blueBase;
    public GameObject errorWindow;
    public GameObject endGameWindow;
    public UISounds uiSounds;
    public PathSwitcher pathSwitcher;
    public Toggle lane1Toggle;
    public Toggle lane2Toggle;
    public Toggle lane3Toggle;
    public static bool healthBarOn = true;

    private PhotonView photonView;
    private bool UIToggled = true;
    private bool speedUp = false;

    void Awake()
    {
        photonView = GetComponent<PhotonView>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            healthBarOn = !healthBarOn;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (optionsMenu.activeSelf)
            {
                optionsMenu.SetActive(false);
            }

            ToggleMenu();
        }

        if (Input.GetKeyDown(KeyCode.F1))
        {
            if (constants.offlineMode)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }

        //if (Input.GetKeyDown(KeyCode.F2))
        //{
        //    mainCamera.transform.position = cameraPositions[0].position;
        //    mainCamera.transform.rotation = cameraPositions[0].rotation;
        //}

        //if (Input.GetKeyDown(KeyCode.F3))
        //{
        //    mainCamera.transform.position = cameraPositions[1].position;
        //    mainCamera.transform.rotation = cameraPositions[1].rotation;
        //}

        //if (Input.GetKeyDown(KeyCode.F4))
        //{
        //    mainCamera.transform.position = cameraPositions[2].position;
        //    mainCamera.transform.rotation = cameraPositions[2].rotation;
        //}

        if (Input.GetKeyDown(KeyCode.F5))
        {
            ToggleUI(UIToggled);
        }

        // More Magic
        if (Input.GetKeyDown(KeyCode.F6))
        {
            if (constants.offlineMode)
            {
                GameObject.FindWithTag("Player1").GetComponent<PlayerVars>().calculateMagic(100f);
            }
        }

        // Speedup
        if (Input.GetKeyDown(KeyCode.F7))
        {
            if (constants.offlineMode)
            {
                speedUp = !speedUp;
                Time.timeScale = speedUp ? 10f : 1f;
            }
        }

        // More Health
        if (Input.GetKeyDown(KeyCode.F8))
        {
            if (constants.offlineMode)
            {
                GameObject.FindWithTag("Player1").GetComponent<PhotonView>().RPC("TakeDamage", PhotonTargets.All, -constants.towerStartHealth);
            }
        }

        // Build all Player1 pieces (depends on wizard)
        if (Input.GetKeyDown(KeyCode.F9))
        {
            if (constants.offlineMode)
            {
                foreach (PlacedMainPiece piece in redBase.GetComponentsInChildren<PlacedMainPiece>())
                {
                    if ((int)PhotonNetwork.player.CustomProperties["selectedWizard"] == 0)
                    {
                        foreach (GameObject go in piece.turbyComponents)
                        {
                            go.GetComponent<Renderer>().enabled = true;
                        }
                    }

                    else //selectedWizard == 1
                    {
                        foreach (GameObject go in piece.takComponents)
                        {
                            go.GetComponent<Renderer>().enabled = true;
                        }
                    }
                }
            }
        }

        // Build all Player2 pieces (always Tak)
        if (Input.GetKeyDown(KeyCode.F10))
        {
            if (constants.offlineMode)
            {
                foreach (PlacedMainPiece piece in blueBase.GetComponentsInChildren<PlacedMainPiece>())
                {
                    foreach (GameObject go in piece.takComponents)
                    {
                        go.GetComponent<Renderer>().enabled = true;
                    }
                }
            }
        }

        // Switch Wizard
        if (Input.GetKeyDown(KeyCode.F11))
        {
            if (constants.offlineMode)
            {
                if ((int)PhotonNetwork.player.CustomProperties["selectedWizard"] == 0)
                {
                    redBase.transform.GetChild(0).GetComponent<TowerBaseModels>().turbyBase.SetActive(false);
                    redBase.transform.GetChild(0).GetComponent<TowerBaseModels>().takBase.SetActive(true);
                    PhotonNetwork.player.SetCustomProperties(new Hashtable() { { "selectedWizard", 1 } });
                }

                else // selectedWizard == 1
                {
                    redBase.transform.GetChild(0).GetComponent<TowerBaseModels>().takBase.SetActive(false);
                    redBase.transform.GetChild(0).GetComponent<TowerBaseModels>().turbyBase.SetActive(true);
                    PhotonNetwork.player.SetCustomProperties(new Hashtable() { { "selectedWizard", 0 } });
                }

                networkInGame.player1Picture.sprite = networkInGame.wizardPictures[(int)PhotonNetwork.player.CustomProperties["selectedWizard"]];
            }
        }

        // Level 2 Lane Switch hotkeys
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                lane1Toggle.isOn = true;
                pathSwitcher.SwitchLane(PhotonNetwork.isMasterClient ? 1 : 3);
                uiSounds.SelectionChange();
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                lane2Toggle.isOn = true;
                pathSwitcher.SwitchLane(2);
                uiSounds.SelectionChange();
            }

            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                lane3Toggle.isOn = true;
                pathSwitcher.SwitchLane(PhotonNetwork.isMasterClient ? 3 : 1);
                uiSounds.SelectionChange();
            }
        }
    }

    public void ToggleMenu()
    {
        if (!errorWindow.activeInHierarchy && !endGameWindow.activeInHierarchy)
        {
            menu.SetActive(!menu.activeInHierarchy);

            if (menu.activeInHierarchy)
            {
                uiSounds.GameMenuOpen();
            }
            else
            {
                uiSounds.GameMenuClose();
            }
        }       
    }

    public void Forfeit()
    {
        ToggleMenu();

        if (PhotonNetwork.player.IsMasterClient)
        {
            player1Vars.GetComponent<PhotonView>().RPC("TakeDamage", PhotonTargets.All, constants.towerStartHealth);
        }

        else
        {
            player2Vars.GetComponent<PhotonView>().RPC("TakeDamage", PhotonTargets.All, constants.towerStartHealth);
        }
    }

    public void ExitGame()
    {
        if (PhotonNetwork.connected)
        {
            PhotonNetwork.Disconnect();
        }

        Application.Quit();
    }

    private void ToggleUI(bool toggled)
    {
        foreach (GameObject go in Canvases)
        {
            go.SetActive(!toggled);                
        }

        UIToggled = !toggled;
    }
}
