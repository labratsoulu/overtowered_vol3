﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSwitch : MonoBehaviour {

    public Image levelBackground;
    public Sprite[] levelBackgrounds;
    public GameObject activeDescription;
    public GameObject[] levelDescriptions;
    public PhotonView lobbyView;
    public int selectedLevel = 0;

    private int levelIndex = 0;

    public void SetupLevelSelection()
    {
        levelIndex = selectedLevel;

        levelBackground.sprite = levelBackgrounds[levelIndex];
        activeDescription.SetActive(false);
        levelDescriptions[levelIndex].SetActive(true);
        activeDescription = levelDescriptions[levelIndex];
    }

    public void PreviousLevel()
    {
        levelIndex--;

        if (levelIndex == -1)
        {
            levelIndex = levelBackgrounds.Length - 1;
        }

        levelBackground.sprite = levelBackgrounds[levelIndex];
        activeDescription.SetActive(false);
        levelDescriptions[levelIndex].SetActive(true);
        activeDescription = levelDescriptions[levelIndex];
    }

    public void NextLevel()
    {
        levelIndex++;

        if (levelIndex == levelBackgrounds.Length)
        {
            levelIndex = 0;
        }

        levelBackground.sprite = levelBackgrounds[levelIndex];
        activeDescription.SetActive(false);
        levelDescriptions[levelIndex].SetActive(true);
        activeDescription = levelDescriptions[levelIndex];
    }

    public void ClickSelect()
    {
        lobbyView.RPC("SelectLevel", PhotonTargets.AllBuffered, levelIndex);
    }
}
