﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class NetworkInGame : MonoBehaviour {

    [SerializeField] GameObject sceneCamera;
    [SerializeField] Text player1Name;
    [SerializeField] Text player2Name;
    [SerializeField] Text player1Health;
    [SerializeField] Text player2Health;
	[SerializeField] Image player1HealthSlider; //min = -330 max = 153 difference = 483
	[SerializeField] Image player2HealthSlider; //min = 787 max = 304 difference = 483
    [SerializeField] Text magicText;
	[SerializeField] Image magicSlider;
    [SerializeField] InputField messageWindow;
    [SerializeField] GameObject gameUI;
    [SerializeField] Transform[] cameraPositions;
    [SerializeField] GameObject errorWindow;
    [SerializeField] GameObject testButtons;
    public Sprite[] wizardPictures;
    public Image player1Picture;
    public Image player2Picture;

    public Constants constants;
    public GameObject textDisplayer;
    public GameObject redTower;
    public GameObject redTowerBase;
    public GameObject blueTower;
    public GameObject blueTowerBase;
    public GameObject towerUI;
    public EndGameManager endGameManager;
    public NarratorSpeech narrator;
    public MonoBehaviour[] scriptsToEnableAtStart;
    public const int messageCount = 6;
    public Transform player1Units;
    public Transform player2Units;

    private PhotonView photonView;
    private Queue<string> messages;

    public Texture2D cursorTexture;
	public MusicManager musicManager;
	public Sprite redBar;
	public Sprite blueBar;

    void Awake()
    {
        // Go back to main menu if we are not connected to Photon.
        if (!PhotonNetwork.connected)
        {
            SceneManager.LoadScene(0);
            return;
        }

		musicManager = GameObject.FindGameObjectWithTag("MusicManager").GetComponent<MusicManager>(); //This should have been instantiated in the lobby scene
        photonView = GetComponent<PhotonView>();
        messages = new Queue<string>(messageCount);
        Cursor.SetCursor(cursorTexture, Vector2.zero, CursorMode.Auto);
    }

    // Subscribe to the sceneloaded event when script is enabled, and unsub when it's disabled
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    // This delegate pretty much replaces the Start() method
    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (!PhotonNetwork.connected)
        {
            SceneManager.LoadScene(0);
            return;
        }

        musicManager.IngameMusic (1); //TODO: Make the music selection dynamic
        player1Health.text = constants.towerStartHealth.ToString();
        player2Health.text = constants.towerStartHealth.ToString();

        if (!PhotonNetwork.offlineMode)
        {
			player1Picture.sprite = wizardPictures[(int)PhotonNetwork.player.CustomProperties["selectedWizard"]];

			if (PhotonNetwork.player.ID == 1)
            {
                player2Picture.sprite = wizardPictures[(int)PhotonPlayer.Find(2).CustomProperties["selectedWizard"]];
            }
            	
			else
            {
                player2Picture.sprite = wizardPictures[(int)PhotonPlayer.Find(1).CustomProperties["selectedWizard"]];
            }				
        }

        if (PhotonNetwork.isMasterClient)
        {
            //photonView.RPC("AddMessage", PhotonTargets.All, "<color=red>" + PhotonNetwork.player.name + "</color> <color=green>joined the game</color>");
            redTower.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.player);
            redTowerBase.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.player);

            if (SceneManager.GetActiveScene().buildIndex == 1)
            {
                PhotonNetwork.Instantiate("PlayerCamera", cameraPositions[1].position, cameraPositions[1].rotation, 0);
            }

            else if (SceneManager.GetActiveScene().buildIndex == 2)
            {
                PhotonNetwork.Instantiate("PlayerCamera2", cameraPositions[1].position, cameraPositions[1].rotation, 0);
            }           
        }

        else
        {
            //photonView.RPC("AddMessage", PhotonTargets.All, "<color=blue>" + PhotonNetwork.player.name + "</color> <color=green>joined the game</color>");
            player1HealthSlider.sprite = blueBar;
            player2HealthSlider.sprite = redBar;
            Color nameColor = new Color();
            ColorUtility.TryParseHtmlString("#0047F8FF", out nameColor);
            player1Name.color = nameColor;
            ColorUtility.TryParseHtmlString("#AB0102FF", out nameColor);
            player2Name.color = nameColor;

            blueTower.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.player);
            blueTowerBase.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.player);

            if (SceneManager.GetActiveScene().buildIndex == 1)
            {
                PhotonNetwork.Instantiate("PlayerCamera", cameraPositions[2].position, cameraPositions[2].rotation, 0);
            }

            else if (SceneManager.GetActiveScene().buildIndex == 2)
            {
                PhotonNetwork.Instantiate("PlayerCamera2", cameraPositions[2].position, cameraPositions[2].rotation, 0);
            }
        }

        sceneCamera.SetActive(false);
        photonView.RPC("UpdatePlayerList_RPC", PhotonTargets.All, PhotonNetwork.player.ID, PhotonNetwork.player.NickName);
        photonView.RPC("UpdateHealthList_RPC", PhotonTargets.All, PhotonNetwork.player.ID, constants.towerStartHealth);
		UpdateMagicValue(constants.magicStartAmount, constants.magicStartAmount);

        //textDisplayer.GetComponent<TextDisplayer>().prepareDisplay("<color=#F31E1E>Waiting for another player...</color>", 40, 0f);
        textDisplayer.GetComponent<PhotonView>().RPC("prepareDisplay", PhotonTargets.All, "<color=#F31E1E>Waiting for another player...</color>", 40, 0f);

        if (PhotonNetwork.isNonMasterClientInRoom)
        {
            photonView.RPC("StartGame", PhotonTargets.All);
        }

		if (PhotonNetwork.offlineMode == true)
        {
			constants.offlineMode = true;
			constants.playersToStart = 1;
			testButtons.SetActive (true);
			StartGame();
		}
    }

    // Display message and clear player fields on d/c
    private void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        gameUI.SetActive(false);
        errorWindow.SetActive(true);  
		narrator.disconnected();
    }

    void OnDisconnectedFromPhoton()
    {
        SceneManager.LoadScene(0);
    }

    public void Disconnect()
    {
        PhotonNetwork.LeaveRoom();
    }

    private void OnLeftRoom()
    {
        SceneManager.LoadScene(0);
    }

    [PunRPC]
    void StartGame()
    {
        foreach(MonoBehaviour script in scriptsToEnableAtStart)
        {
            script.enabled = true;
        }

        gameUI.SetActive(true);
        textDisplayer.GetComponent<PhotonView>().RPC("prepareDisplay", PhotonTargets.All, "<color=#63FF77FF>Fight!</color>", 80, 3f);
		narrator.gameStart();
    }

    [PunRPC]
    void AddMessage(string message)
    {
        messages.Enqueue(message);

        if (messages.Count > messageCount)
        {
            messages.Dequeue();
        }

        messageWindow.text = "";

        foreach (string m in messages)
        {
            messageWindow.text += m + "\n";
        }      
    }

    [PunRPC]
    void UpdatePlayerList_RPC(int playerID, string name)
    {
		if (playerID == PhotonNetwork.player.ID)
        {
            player1Name.text = name;
        }

        else //playerID == 2
        {
            player2Name.text = name;
        }
    }

    [PunRPC]
    void UpdateHealthList_RPC(int playerID, float health)
    {
		if (playerID == PhotonNetwork.player.ID)
        {
            player1Health.text = health.ToString();
			Vector2 pos = player1HealthSlider.GetComponent<RectTransform>().anchoredPosition;
			pos.x = (483 / constants.towerStartHealth * health) -330;
			player1HealthSlider.GetComponent<RectTransform> ().anchoredPosition = pos;
        }

        else 
        {
            player2Health.text = health.ToString();
			Vector2 pos = player2HealthSlider.GetComponent<RectTransform>().anchoredPosition;
			pos.x = 483 - (483 / constants.towerStartHealth * health) + 304;
			player2HealthSlider.GetComponent<RectTransform> ().anchoredPosition = pos;
        }
    }

	public void UpdateMagicValue(float magic, float maxMagic)
    {
		magicText.text = magic.ToString();
		//magicSlider.color = new Color(1, 1, 1, magic / 100);
		Vector3 scale = new Vector3(1, magic / maxMagic, 1);
		magicSlider.GetComponent<RectTransform> ().localScale = scale;
    }

    [PunRPC]
    public void EndGame(int playerID)
    {
        if (playerID == PhotonNetwork.player.ID)
        {
			musicManager.DefeatMusic();
			narrator.gameOver(false);
        }

        else
        {
            musicManager.VictoryMusic();
			narrator.gameOver(true);
        }

        foreach (Transform unit in player1Units)
        {
            unit.GetComponent<UnitAI>().Stun(666);
        }

        foreach (Transform unit in player2Units)
        {
            unit.GetComponent<UnitAI>().Stun(666);
        }

        gameUI.SetActive(false);
        endGameManager.SetupEndGameWindow(playerID);
    }
}
