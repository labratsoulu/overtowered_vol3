﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class NetworkLobby : MonoBehaviour {

    public Constants constants;

    [SerializeField] Camera sceneCamera;
    [SerializeField] GameObject mainMenu;
    [SerializeField] GameObject lobbyWindow;
    [SerializeField] GameObject createRoomWindow;
    [SerializeField] GameObject joinPrivateWindow;
    [SerializeField] GameObject roomWindow;
    [SerializeField] GameObject wizardSelectionWindow;
    [SerializeField] GameObject levelSelectionWindow;
    [SerializeField] GameObject errorWindow;
    [SerializeField] GameObject creditsWindow;
    [SerializeField] GameObject optionsWindow;
    [SerializeField] GameObject tutorialWindow;
    [SerializeField] Text errorText;
    [SerializeField] ScrollRect roomList;
    [SerializeField] InputField newRoomName;
    [SerializeField] InputField privateRoomName;
    [SerializeField] Text roomNameText;
    [SerializeField] InputField playerName;
    [SerializeField] Text player1Name;
    [SerializeField] Text player2Name;
    [SerializeField] Image player1Wizard;
    [SerializeField] Image player2Wizard;
    [SerializeField] Image levelImage;
    [SerializeField] Toggle privateToggle;
    [SerializeField] GameObject roomLockImage;
    [SerializeField] GameObject player1ReadyCheck;
    [SerializeField] GameObject player2ReadyCheck;
    [SerializeField] Button startGameButton;
    [SerializeField] Text connectionText;
    [SerializeField] Text versionNumberText;

    public GameObject roomButton;
    public UISounds soundManager;
    public WizardSwitch wizardSwitch;
    public LevelSwitch levelSwitch;
    public Texture2D cursorTexture;

    private Queue<string> messages;
    private const int messageCount = 6;
    private PhotonView photonView;
    private bool isRecreating = false;
    private string reRoomName;
    private RoomOptions reOptions;

	private MusicManager musicManager;

    // Use this for initialization
    void Awake()
    {
		musicManager = GameObject.FindGameObjectWithTag ("MusicManager").GetComponent<MusicManager> ();
		if (musicManager != null) {
			musicManager.MenuMusic ();
		}
        versionNumberText.text = constants.versionNumber;
        photonView = GetComponent<PhotonView>();
        PhotonNetwork.logLevel = PhotonLogLevel.Full;
        //PhotonNetwork.autoCleanUpPlayerObjects = false;

        Cursor.SetCursor(cursorTexture, Vector2.zero, CursorMode.Auto);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (errorWindow.activeSelf)
            {
                ReturnFromErrorWindow();
                soundManager.MenuBack();
            }

            else if (tutorialWindow.activeSelf)
            {
                tutorialWindow.SetActive(false);
                soundManager.MenuBack();
            }

            else if (creditsWindow.activeSelf)
            {
                creditsWindow.SetActive(false);
                soundManager.MenuBack();
            }

            else if (optionsWindow.activeSelf)
            {
                optionsWindow.SetActive(false);
                soundManager.MenuBack();
            }

            else if (createRoomWindow.activeSelf)
            {
                createRoomWindow.SetActive(false);
                soundManager.MenuBack();
            }

            else if (joinPrivateWindow.activeSelf)
            {
                joinPrivateWindow.SetActive(false);
                soundManager.MenuBack();
            }

            else if (lobbyWindow.activeSelf)
            {
                ReturnToMainMenu();
                soundManager.MenuBack();
            }
            
            else if (wizardSelectionWindow.activeSelf)
            {
                wizardSelectionWindow.SetActive(false);
                soundManager.MenuBack();
            }

            else if (levelSelectionWindow.activeSelf)
            {
                levelSelectionWindow.SetActive(false);
                soundManager.MenuBack();
            }

            else if (roomWindow.activeSelf)
            {
                LeaveRoom();
                soundManager.HeavyClick();
            }

            else
            {
                soundManager.ExitMatch();
                ExitGame();
            }
        }
    }

    private IEnumerator UpdateConnectionString()
    {
        while (true)
        {
            connectionText.text = PhotonNetwork.connectionStateDetailed.ToString();
            yield return null;
        }
    }

    public void StartTestMode(int lvlID)
    {
        PhotonNetwork.offlineMode = true;
        PhotonNetwork.player.NickName = "TestWizard";
        RoomOptions ro = new RoomOptions() { IsOpen = false, IsVisible = false, MaxPlayers = 1 };
        PhotonNetwork.CreateRoom("testroom", ro, TypedLobby.Default);
        PhotonNetwork.player.SetCustomProperties(new Hashtable() { { "selectedWizard", 0 } });
        PhotonNetwork.player.SetCustomProperties(new Hashtable() { { "score", 0 } });
        PhotonNetwork.LoadLevel(lvlID);
    }

    public void JoinLobby()
    {
        PhotonNetwork.offlineMode = false;
        PhotonNetwork.ConnectUsingSettings(constants.versionNumber);
        StartCoroutine("UpdateConnectionString");
        PhotonNetwork.autoJoinLobby = true;
        PhotonNetwork.automaticallySyncScene = true;
    }

    private void OnJoinedLobby()
    {
        if (isRecreating)
        {
            PhotonNetwork.CreateRoom(reRoomName, reOptions, TypedLobby.Default);
        }

        else
        {
            lobbyWindow.SetActive(true);
            mainMenu.SetActive(false);
            StopCoroutine("UpdateConnectionString");
            connectionText.text = "";
            PhotonNetwork.player.SetCustomProperties(new Hashtable() { { "selectedWizard", 0 } });
            PhotonNetwork.player.SetCustomProperties(new Hashtable() { { "score", 0 } });
        }    
    }

    private void OnReceivedRoomListUpdate()
    {
        foreach (Transform child in roomButton.transform.parent.transform)
        {
            if (child.gameObject.activeSelf)
            {
                Destroy(child.gameObject);
            }
        }

        RoomInfo[] rooms = PhotonNetwork.GetRoomList();

        foreach (RoomInfo room in rooms)
        {
            if (room.PlayerCount < room.MaxPlayers)
            {
                GameObject roomBtn = Instantiate(roomButton);
                roomBtn.SetActive(true);
                roomBtn.transform.SetParent(roomButton.transform.parent, false);
                roomBtn.GetComponent<RoomButtonClick>().roomName = room.Name;
                roomBtn.GetComponentInChildren<Text>().text = room.Name;
            }        
        }
    }

    // Buttons
    public void OpenCreateGameWindow()
    {
        if (NameNotEmpty())
        {
            createRoomWindow.SetActive(true);
            newRoomName.Select();
            newRoomName.ActivateInputField();
        }       
    }

    public void OpenJoinPrivateWindow()
    {
        if (NameNotEmpty())
        {
            joinPrivateWindow.SetActive(true);
            privateRoomName.Select();
            privateRoomName.ActivateInputField();
        }       
    }

    public void CreateRoom()
    {
        PhotonNetwork.player.NickName = playerName.text;
        RoomOptions ro = new RoomOptions() { IsOpen = true, IsVisible = !privateToggle.isOn, MaxPlayers = constants.maxPlayers };
        PhotonNetwork.CreateRoom(newRoomName.text, ro, TypedLobby.Default);
        createRoomWindow.SetActive(false);   
    }

    public void RecreateRoom()
    {
        isRecreating = true;
        reRoomName = PhotonNetwork.room.Name;
        reOptions = new RoomOptions() { IsOpen = true, IsVisible = PhotonNetwork.room.IsVisible, MaxPlayers = constants.maxPlayers };
        PhotonNetwork.LeaveRoom();
    }

    public void JoinSelectedRoom(string roomName)
    {
        if (NameNotEmpty())
        {
            PhotonNetwork.player.NickName = playerName.text;
            PhotonNetwork.JoinRoom(roomName);
        }       
    }

    public void JoinPrivateRoom()
    {
        if (PrivateRoomNameNotEmpty())
        {
            PhotonNetwork.player.NickName = playerName.text;
            PhotonNetwork.JoinRoom(privateRoomName.text);
        }   
    }

    public void JoinRandomRoom()
    {
        if (NameNotEmpty())
        {
            PhotonNetwork.player.NickName = playerName.text;
            PhotonNetwork.JoinRandomRoom();
        }
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
        roomWindow.SetActive(false);
        lobbyWindow.SetActive(true);
    }

    public void ReturnToMainMenu()
    {
        PhotonNetwork.LeaveLobby();
        PhotonNetwork.Disconnect();
        lobbyWindow.SetActive(false);
        mainMenu.SetActive(true);
    }

    public void ReturnFromErrorWindow()
    {
        errorWindow.SetActive(false);

        if (!PhotonNetwork.connected)
        {
            roomWindow.SetActive(false);
            wizardSelectionWindow.SetActive(false);
            joinPrivateWindow.SetActive(false);
            createRoomWindow.SetActive(false);
            lobbyWindow.SetActive(false);
            mainMenu.SetActive(true);
        }
    }

    public void ExitGame()
    {
        if (PhotonNetwork.connected)
        {
            PhotonNetwork.Disconnect();
        }

        Application.Quit();
    }

    // Failures
    private bool NameNotEmpty()
    {
        if (playerName.text == "")
        {
            errorText.text = "Player name cannot be empty. Please input a valid name.";
            errorWindow.SetActive(true);
            return false;
        }

        return true;
    }

    private bool PrivateRoomNameNotEmpty()
    {
        if (privateRoomName.text == "")
        {
            errorText.text = "Type the name of the private room you wish to join.";
            errorWindow.SetActive(true);
            return false;
        }

        return true;
    }

    private void OnPhotonCreateRoomFailed()
    {
        errorText.text = "Failed to create room. Most likely a room with that name already exists. Please try again.";
        errorWindow.SetActive(true);
    }

    private void OnPhotonJoinRoomFailed()
    {
        errorText.text = "Failed to join selected room. No game with the selected name exists or the game already started. Please try again.";
        errorWindow.SetActive(true);     
    }

    private void OnPhotonRandomJoinFailed()
    {
        //errorText.text = "Failed to join a random game. Perhaps there are no available games right now. Please try again or create a new game.";
        //errorWindow.SetActive(true);

        //Instead of giving an error, let's make a new room.
        PhotonNetwork.player.NickName = playerName.text;
        RoomOptions ro = new RoomOptions() { IsOpen = true, IsVisible = true, MaxPlayers = constants.maxPlayers };
        PhotonNetwork.CreateRoom("", ro, TypedLobby.Default);
        createRoomWindow.SetActive(false);
    }

    private void OnConnectionFail()
    {
        errorText.text = "Connection lost. Please check your internet connection and try again.";
        errorWindow.SetActive(true);
    }

    private void OnFailedToConnectToPhoton(DisconnectCause cause)
    {
        if (cause == DisconnectCause.MaxCcuReached || cause == DisconnectCause.DisconnectByServerUserLimit)
        {
            errorText.text = "Server is full. Please try again later.";
        }

        else if (cause == DisconnectCause.ExceptionOnConnect)
        {
            errorText.text = "Server is down at the moment, possibly due to maintenance. Please try again later.";
        }

        else
        {
            errorText.text = "Failed to connect to server. Please check your internet connection and try again.";
        }

        errorWindow.SetActive(true);
        StopCoroutine("UpdateConnectionString");
        connectionText.text = "";
    }

    private void OnJoinedRoom()
    {
        if (PhotonNetwork.offlineMode == true)
        {
            return;
        }

        isRecreating = false;
        lobbyWindow.SetActive(false);
        roomWindow.SetActive(true);

        foreach(PhotonPlayer player in PhotonNetwork.playerList)
        {
            photonView.RPC("ToggleReadyMark", PhotonTargets.All, player.ID, false);
        }        

        if (PhotonNetwork.playerList.Length == 1)
        {
            if (PhotonNetwork.room.IsVisible)
            {
                photonView.RPC("ChangeRoomText", PhotonTargets.AllBuffered, PhotonNetwork.room.Name + " (Public)");
            }

            else
            {
                photonView.RPC("ChangeRoomText", PhotonTargets.AllBuffered, PhotonNetwork.room.Name + " (Private)");
                roomLockImage.SetActive(true);
            }

            photonView.RPC("UpdatePlayerList", PhotonTargets.AllBuffered, PhotonNetwork.player.ID);
        }
    }

    private void OnPhotonPlayerConnected(PhotonPlayer joinedPlayer)
    {
        if (PhotonNetwork.playerList.Length == 2)
        {
            photonView.RPC("UpdatePlayerList", PhotonTargets.All, joinedPlayer.ID);
        }

        print("Masterclient ID = " + PhotonNetwork.masterClient.ID);

        foreach (PhotonPlayer player in PhotonNetwork.playerList)
        {
            print("Player ID = " + player.ID + " || Player Name = " + player.NickName);
        }
    }

    private void OnPhotonPlayerDisconnected(PhotonPlayer leftPlayer)
    {
        photonView.RPC("UpdatePlayerList", PhotonTargets.All, leftPlayer.ID);
        player1ReadyCheck.SetActive(false);
        player2ReadyCheck.SetActive(false);
        RecreateRoom();
    }

    public void OpenWizardSelectionWindow()
    {
        photonView.RPC("ToggleReadyMark", PhotonTargets.All, PhotonNetwork.player.ID, false);
        wizardSelectionWindow.SetActive(true);
    }

    public void OpenLevelSelectionWindow()
    {
        photonView.RPC("ToggleReadyMark", PhotonTargets.All, PhotonNetwork.player.ID, false);
        levelSelectionWindow.SetActive(true);
    }

    public void ClickWizard1Button()
    {
        if (PhotonNetwork.isMasterClient)
        {
            soundManager.MenuForward();
            photonView.RPC("ToggleReadyMark", PhotonTargets.All, PhotonNetwork.player.ID, false);
            wizardSelectionWindow.SetActive(true);
        }
    }

    public void ClickWizard2Button()
    {
        if (!PhotonNetwork.isMasterClient)
        {
            soundManager.MenuForward();
            photonView.RPC("ToggleReadyMark", PhotonTargets.All, PhotonNetwork.player.ID, false);
            wizardSelectionWindow.SetActive(true);
        }
    }

    public void SelectWizard()
    {
        PhotonNetwork.player.SetCustomProperties(new Hashtable() { { "selectedWizard", wizardSwitch.selectedIndex } });
        photonView.RPC("UpdatePlayerList", PhotonTargets.AllBuffered, PhotonNetwork.player.ID);
        wizardSelectionWindow.SetActive(false);
    }

    [PunRPC]
    public void SelectLevel(int lvlID)
    {
        levelSwitch.selectedLevel = lvlID;
        levelImage.sprite = levelSwitch.levelBackgrounds[lvlID];
        levelSelectionWindow.SetActive(false);
    }

    public void StartGameClick()
    {
        if (PhotonNetwork.playerList.Length == PhotonNetwork.room.MaxPlayers)
        {
            soundManager.SelectionChange();
            photonView.RPC("ToggleReadyMark", PhotonTargets.All, PhotonNetwork.player.ID, true);

            if (player1ReadyCheck.activeInHierarchy && player2ReadyCheck.activeInHierarchy)
            {
                photonView.RPC("StartGame", PhotonTargets.All);
            }
        }     
    }

    [PunRPC]
    public void ToggleReadyMark(int playerID, bool isOpposite)
    {
        if (PhotonNetwork.masterClient.ID == playerID)
        {
            player1ReadyCheck.SetActive(isOpposite ? !player1ReadyCheck.activeInHierarchy : false);           
        }

        else // ID == 2
        {
            player2ReadyCheck.SetActive(isOpposite ? !player2ReadyCheck.activeInHierarchy : false);
        }
    }

    [PunRPC]
    public void StartGame()
    {
        soundManager.GameStart();

        if (PhotonNetwork.player.IsMasterClient)
        {
            PhotonNetwork.room.IsVisible = false;
            PhotonNetwork.room.IsOpen = false;
            PhotonNetwork.LoadLevel(levelSwitch.selectedLevel + 1);
        }
    }

    [PunRPC]
    public void UpdatePlayerList(int playerID)
    {
        player1Name.text = PhotonNetwork.masterClient.NickName;
        player1Wizard.sprite = wizardSwitch.wizardPictures[(int)PhotonNetwork.masterClient.CustomProperties["selectedWizard"]];

        if (PhotonNetwork.playerList.Length == 1)
        {
            player2Name.text = "<color=red>Waiting...</color>";
            player2Wizard.sprite = wizardSwitch.wizardPictures[0];
        }

        if (PhotonNetwork.room.PlayerCount == 2)
        {
            player2Name.text = PhotonPlayer.Find(2).NickName;
            player2Wizard.sprite = wizardSwitch.wizardPictures[(int)PhotonPlayer.Find(2).CustomProperties["selectedWizard"]];
        }               
    }

    [PunRPC]
    public void ChangeRoomText(string name)
    {
        roomNameText.text = name;
    }

    [PunRPC]
    public void AddMessage(string message)
    {
        messages.Enqueue(message);

        if (messages.Count > messageCount)
        {
            messages.Dequeue();
        }

        //messageWindow.text = "";

        foreach (string m in messages)
        {
            //messageWindow.text += m + "\n";
        }
    }
}
