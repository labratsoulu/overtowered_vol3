﻿using UnityEngine;
using System.Collections;

public class AbstractTowerPiece : MonoBehaviour {

	public enum blockstate { normal, dragged };
	public blockstate state;
	public Color originalColor;
	public Color toColor;

	protected Renderer rend;
	protected RaycastHit hit;
	protected float posX;
	protected float posY;
	protected float posZ;

	void Awake () {
		rend = GetComponent<Renderer>();
	}

	void LateUpdate () {
		switch (state)
		{
		case blockstate.normal:
			break;

		case blockstate.dragged:
			Dragged();
			break;

		default:
			break;
		}
	}

	public void StartDrag()
	{
		//rend.material.color = toColor;

		//lets make ray go thru this object
		gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
		foreach(Transform child in transform){
			child.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
		}
		state = blockstate.dragged;
	}

	protected virtual void Dragged()
	{
		//posX = Input.GetAxis("Mouse X");
		//posY = Input.GetAxis("Mouse Y");
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		Debug.DrawRay(ray.origin, ray.direction*100, Color.green);

		if (Physics.Raycast(ray, out hit))
		{
			posZ = hit.point.z;
			posX = hit.point.x;
			posY = hit.point.y;

			posY += gameObject.transform.localScale.y;
		}
		transform.position = new Vector3(posX, posY, posZ);
	}

	protected virtual void EndDrag()
	{
		state = blockstate.normal;
		//rend.material.color = originalColor;
		gameObject.layer = LayerMask.NameToLayer("Default");
		foreach(Transform child in transform){
			child.gameObject.layer = LayerMask.NameToLayer("Default");
		}
		//this message is used to activate thing in spawner,adder scripts
		//SendMessage("ActivateModule");
	}
}
