﻿using UnityEngine;
using System.Collections;

public class ConnectorPiece : AbstractTowerPiece {

	private bool mouseClick;

	public GameObject Piece;

    void Awake()
    {
        //gameObject.GetComponent<PhotonView>().synchronization = ViewSynchronization.Off;
    }

    protected override void Dragged(){
		base.Dragged ();
		if (hit.transform != null) {
			//if to a node, turn to correct angle and hold
			if (hit.transform.tag == "node") {
				transform.rotation = hit.transform.rotation;
				transform.position = new Vector3 (hit.transform.position.x, hit.transform.position.y, hit.transform.position.z);
			} else {
				transform.rotation = Quaternion.Euler (0f, 0f, 0f);
			}
		}

		if (Input.GetMouseButtonUp(0))
		{
			mouseClick = false;
		}
		if (mouseClick==false)
		{
			EndDrag();
		}
	}

	protected override void EndDrag(){
		
        //gameObject.GetComponent<PhotonView>().synchronization = ViewSynchronization.UnreliableOnChange;

        if (Input.GetMouseButtonDown(0) && state == blockstate.dragged) {
			
			if (hit.transform != null && hit.transform.tag == "node") {
				GameObject piece;
				piece = PhotonNetwork.Instantiate (Piece.name, transform.position, Quaternion.identity, 0);
				piece.GetComponent<PhotonView> ().RPC ("setAllParents", PhotonTargets.Others, hit.transform.parent.gameObject.GetComponent<PieceID>().ID, hit.transform.GetSiblingIndex());
				piece.transform.root.SetParent (hit.transform);
				piece.transform.rotation = hit.transform.rotation;
				hit.transform.gameObject.GetComponent<CapsuleCollider> ().enabled = false;
				base.EndDrag ();
			}
            Destroy(gameObject);
		} else if (Input.GetMouseButtonDown (1)) {
			Destroy (gameObject);
		}
	}
}
