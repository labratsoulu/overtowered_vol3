﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class PathToggler : MonoBehaviour {

    public Constants constants;
    public PlayerVars player1Vars;
    public PlayerVars player2Vars;
    public GameObject player1Units;
    public GameObject player2Units;
    public GameObject redBridge;
    public GameObject blueBridge;
    public float toggleCost = 50f;
    public int controllerID;
    public float dropAreaX;
    public float dropAreaZ;
    public float dropStunDuration;

    private PhotonView thisView;
    private PlayerVars playerVars;
    private GameObject ownUnits;
    private GameObject enemyUnits;
    private Collider[] dropColliders;

    public LayerMask dropMask;

	[FMODUnity.EventRef]
	public string bridgeAppearEvent = "event:/spells/bridge_on";

	[FMODUnity.EventRef]
	public string bridgeTakeoverEvent = "event:/spells/bridge_switch";

	[FMODUnity.EventRef]
	public string bridgeDestroyEvent = "event:/spells/bridge_off";


	void Awake()
    {
        controllerID = 0;
        thisView = gameObject.GetComponent<PhotonView>();

        if (PhotonNetwork.player.ID == 1)
        {
            playerVars = player1Vars;
        }
        
        else // playerID == 2
        {
            playerVars = player2Vars;
        }
    }

    public void Toggle()
    {
        if (controllerID == PhotonNetwork.player.ID)
        {
            thisView.RPC("DestroyBridge", PhotonTargets.All, PhotonNetwork.player.ID);           
        }
        
        else
        {
            if (playerVars.magic >= toggleCost)
            {
                playerVars.calculateMagic(-toggleCost);
                thisView.RPC("TakeBridge", PhotonTargets.All, PhotonNetwork.player.ID);
            }
        }    
    }

    [PunRPC]
    private void TakeBridge(int playerID)
    {
        controllerID = playerID;       

        if (playerID == 1)
        {
            ownUnits = player1Units;
            enemyUnits = player2Units;
            redBridge.SetActive(true);
            blueBridge.SetActive(false);
        }

        else // playerID == 2 
        {
            ownUnits = player2Units;
            enemyUnits = player1Units;
            blueBridge.SetActive(true);
            redBridge.SetActive(false);
        }

        foreach (Transform unit in ownUnits.transform)
        {
            unit.GetComponent<NavMeshAgent>().areaMask = 9;

            foreach(Material mat in unit.GetComponentInChildren<SkinnedMeshRenderer>().materials)
            {
                mat.renderQueue = 2452;
            }
        }

        foreach (Transform unit in enemyUnits.transform)
        {
            unit.GetComponent<NavMeshAgent>().areaMask = 1;

            foreach (Material mat in unit.GetComponentInChildren<SkinnedMeshRenderer>().materials)
            {
                mat.renderQueue = 2450;
            }
        }

		BridgeSound (false);
        DropUnits();       
    }

    [PunRPC]
    private void DestroyBridge(int playerID)
    {
        controllerID = 0;

        if (playerID == 1)
        {
            redBridge.SetActive(false);
            ownUnits = player1Units;
        }

        else // playerID == 2 
        {
            blueBridge.SetActive(false);
            ownUnits = player2Units;
        }

        foreach (Transform unit in ownUnits.transform)
        {
            unit.GetComponent<NavMeshAgent>().areaMask = 1;

            foreach (Material mat in unit.GetComponentInChildren<SkinnedMeshRenderer>().materials)
            {
                mat.renderQueue = 2450;
            }
        }

		BridgeSound (true);
        DropUnits();
    }

    private void DropUnits()
    {
        dropColliders = Physics.OverlapBox(transform.position, new Vector3(dropAreaX, 1, dropAreaZ), Quaternion.identity, dropMask);

        foreach (Collider unit in dropColliders)
        {
            NavMeshAgent agent = unit.gameObject.GetComponent<NavMeshAgent>();

            if (agent.areaMask == 1)
            {
                UnitAI unitAI = unit.gameObject.GetComponent<UnitAI>();
                agent.enabled = false;
                unitAI.Stun(dropStunDuration);
                unit.gameObject.AddComponent<Rigidbody>();
            }
        }          
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireCube(Vector3.zero, new Vector3(dropAreaX * 2f, 2, dropAreaZ * 2f));
    }

	private void BridgeSound(bool destroying) {
		if (!destroying) {
			if (!redBridge.activeInHierarchy && !blueBridge.activeInHierarchy) {
				if (FmodEventChecker.EventExists (bridgeAppearEvent)) {
					FMODUnity.RuntimeManager.PlayOneShot (bridgeAppearEvent, redBridge.transform.position);
				}
			} else {
				if (FmodEventChecker.EventExists (bridgeTakeoverEvent)) {
					FMODUnity.RuntimeManager.PlayOneShot (bridgeTakeoverEvent, redBridge.transform.position);
				}
			}
		} else {
			if (FmodEventChecker.EventExists (bridgeDestroyEvent)) {
				FMODUnity.RuntimeManager.PlayOneShot (bridgeDestroyEvent, redBridge.transform.position);
			}
		}
	}
}
