﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderSounds : MonoBehaviour {

	UISounds uiSounds;
	bool muted = false;
	Slider slider;

	// Use this for initialization
	void Awake () {
		uiSounds = GameObject.FindGameObjectWithTag ("UISoundManager").GetComponent<UISounds> ();
		slider = gameObject.GetComponent<Slider> ();
		if (slider != null) {
			//Debug.Log (slider);
			slider.onValueChanged.AddListener (delegate { ValueChange (); } );

		}

	}

	public void Mute() {
		muted = true;
	}

	public void UnMute() {
		muted = false;
	}

	void ValueChange() {
		if (!muted) {
			uiSounds.HoverEnter ();
		}
	}

	void OnInitializePotentialDrag() {
		if (!muted) {
			//uiSounds.SelectionChange ();
			uiSounds.TestCoin();
		}
	}

	void OnEndDrag() {
		if (!muted) {
			//uiSounds.SelectionChange ();
			uiSounds.TestCoin();
		}
	}
}
