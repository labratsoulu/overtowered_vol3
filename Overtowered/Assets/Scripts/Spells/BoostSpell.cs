﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class BoostSpell : MonoBehaviour {

	protected RaycastHit hit;

	public TowerPieceUI piece;

	[FMODUnity.EventRef]
	public string sfxEvent = "event:/spells/stat_buff";

    //	[FMODUnity.EventRef]
    //	public string voEvent = "event:/Spells/Speeding unit";

    private Camera cam;
    private GameObject playerCamera;
    private float spellCost;
    private Constants constants;
	private LayerMask unitLayer;
	private PlayerVars playerVars;
    private PhotonView animView;
    private string spellsound;
    private Vector3 spellPosition;
    private Projector spellProjector;

	void Awake ()
	{
		//print((int)statChange + " ast");
		if (PhotonNetwork.player.ID == 1)
		{
			unitLayer = LayerMask.NameToLayer("Player1");
            playerVars = GameObject.FindWithTag("Player1").GetComponent<PlayerVars>();
            playerCamera = GameObject.FindWithTag("Player1Camera");
        }

		else // playerID == 2
		{
			unitLayer = LayerMask.NameToLayer("Player2");
            playerVars = GameObject.FindWithTag("Player2").GetComponent<PlayerVars>();
            playerCamera = GameObject.FindWithTag("Player2Camera");
        }

        cam = playerCamera.GetComponent<Camera>();
        animView = playerCamera.GetComponent<PhotonView>();
        constants = GameObject.FindGameObjectWithTag("Constants").GetComponent<Constants>();
        spellCost = constants.boostSpellCost;
		spellsound = "event:/Spells/Speeding unit";
        spellProjector = GetComponent<Projector>();
	}

	// Update is called once per frame
	void Update ()
	{
		Ray ray = cam.ScreenPointToRay(Input.mousePosition);

		if (Physics.Raycast(ray, out hit))
		{
			transform.position = new Vector3(hit.point.x, hit.point.y, hit.point.z);
		}

        StartCoroutine("EndDrag");
    }

    protected IEnumerator EndDrag()
    {
		if (Input.GetMouseButtonDown (0) && !EventSystem.current.IsPointerOverGameObject())
        {
			GameObject.FindGameObjectWithTag ("narrator").GetComponent<NarratorSpeech> ().casting ();
			//FMODUnity.RuntimeManager.PlayOneShot (spellsound, transform.position);
			playerVars.calculateMagic(Mathf.Round(-constants.boostSpellCost * piece.spellCostDown));
			piece.resetTimer ();

			if (hit.transform != null)
            {
                spellPosition = transform.position;
                spellProjector.enabled = false;
                	
                animView.RPC("SetAnimatorTrigger", PhotonTargets.All, "BoostCast");
                yield return new WaitForSeconds(0.5f);
                PhotonNetwork.Instantiate("BoostSpellEffect", spellPosition + new Vector3(0, 3f, 0), Quaternion.Euler(90, 0, 0), 0);

                FMOD.Studio.EventInstance sfxInstance = null;
				if (FmodEventChecker.EventExists (sfxEvent)) {
					sfxInstance = FMODUnity.RuntimeManager.CreateInstance (sfxEvent);
				}
				if(sfxInstance != null) {
					sfxInstance.set3DAttributes (FMODUnity.RuntimeUtils.To3DAttributes (hit.transform));
					sfxInstance.start ();
					sfxInstance.release ();
				}

				Collider[] hitColliders = Physics.OverlapSphere (spellPosition, constants.spellRadius);

				foreach (Collider unit in hitColliders)
                {
					if (unit.gameObject.layer == unitLayer)
                    {
						unit.gameObject.GetComponent<PhotonView>().RPC("boostUnit", PhotonTargets.All, piece.spellEffectUp);
						unit.gameObject.GetComponent<PhotonView>().RPC("ActivateEffect", PhotonTargets.All, 0);
					} 
				}
			}

			Destroy(gameObject);
		}

        else if (Input.GetMouseButtonDown(1))
        {
			Destroy(gameObject); 
		}
	}
}
