﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleColorChange : MonoBehaviour {

    private Toggle toggle;
    private ColorBlock toggledColors;

    void Awake()
    {
        toggle = GetComponent<Toggle>();
        toggledColors = toggle.colors;
    }

	public void OnValueChanged()
    {
        if (toggle.isOn)
        {
            toggledColors.normalColor = Color.black;
            toggle.colors = toggledColors;
        }

        else
        {
            toggledColors.normalColor = Color.white;
            toggle.colors = toggledColors;
        }
    }
}
