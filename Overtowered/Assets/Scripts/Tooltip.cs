﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//transform.position = Input.mousePosition;
	}

	public void showTooltip(int tooltipIndex){
		var temp = GameObject.FindGameObjectWithTag ("tooltip");
		temp.GetComponent<Image> ().enabled = true;

        switch (tooltipIndex)
        {
            case 0:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "<size=30>Unit      <color=black>Cost:</color> 75</size>\n\n" +
                    "<color=black><size=30>Emmett</size></color>\n\n" +
                    "<color=black>Unit Value:</color> 5\n" +
                    "<color=black>Spawn time:</color> 25\n" +
                    "<color=black>Attack:</color> Melee, AoE\n" +
                    "<color=black>Adjacency bonus:</color> Emmett spawn time -20%\n\n" +
                    "Spawns Emmett, a powerful melee unit every 25 seconds.";
                break;

            case 1:               
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "<size=30>Unit      <color=black>Cost:</color> 25</size>\n\n" +
                    "<color=black><size=30>Sarko</size></color>\n\n" +
                    "<color=black>Unit Value:</color> 1\n" +
                    "<color=black>Spawn time:</color> 10\n" +
                    "<color=black>Attack:</color> Melee\n" +
                    "<color=black>Special:</color> Rushes enemy tower at 50% health\n" +
                    "<color=black>Adjacency bonus:</color> Sarko spawn time -20%\n\n" +
                    "Spawns Sarko, a basic melee unit every 10 seconds.";
                break;

            case 2:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "<size=30>Unit      <color=black>Cost:</color> 15</size>\n\n" +
                    "<color=black><size=30>Lamiel</size></color>\n\n" +
                    "<color=black>Unit Value:</color> 1\n" +
                    "<color=black>Spawn time:</color> 10\n" +
                    "<color=black>Attack:</color> Ranged\n" +
                    "<color=black>Adjacency bonus:</color> Lamiel spawn time -20%\n\n" +
                    "Spawns Lamiel, a weak ranged unit every 10 seconds.";
                break;

            case 3:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "<size=30>Utility   <color=black>Cost:</color> 50</size>\n\n" +
                    "<color=black><size=30>Brew Cellar</size></color>\n\n" +
                    "<color=black>Effect:</color> Max magic +100\n" +
                    "<color=black>Adjacency bonus:</color> Spell cooldown -20%\n\n" +
                    "Passively increases maximum magic by 100.";
                break;

            case 4:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "<size=30>Spell     <color=black>Cost:</color> 25</size>\n\n" +
                    "<color=black><size=30>Volatile Brew</size></color>\n\n" +
                    "<color=black>Spell cost:</color> 30\n" +
                    "<color=black>Cooldown:</color> 20\n" +
                    "<color=black>Effect:</color> Damage + knockback\n" +
                    "<color=black>Adjacency bonus:</color> Unit dmg +20%\n\n" +
                    "Casts a spell that deals damage to enemies and throws them around.";
                break;

            case 5:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "<size=30>Utility   <color=black>Cost:</color> 50</size>\n\n" +
                    "<color=black><size=30>Automated Brewery</size></color>\n\n" +
                    "<color=black>Effect:</color> Magic regen +50%\n" +
                    "<color=black>Adjacency bonus:</color> Spell effect +25%\n\n" +
                    "Passively increases magic regeneration by 50%.";
                break;

            case 6:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "<size=30>Spell     <color=black>Cost:</color> 25</size>\n\n" +
                    "<color=black><size=30>Minion Fertilizer</size></color>\n\n" +
                    "<color=black>Spell cost:</color> 30\n" +
                    "<color=black>Cooldown:</color> 20\n" +
                    "<color=black>Effect:</color> Unit stats +50%\n" +
                    "<color=black>Duration:</color> 15\n" +
                    "<color=black>Adjacency bonus:</color> Spell effect +25%\n\n" +
                    "Casts a spell that boosts the stats and size of your units.";
                break;

            case 7:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "<size=30>Utility   <color=black>Cost:</color> 50</size>\n\n" +
                    "<color=black><size=30>Explosive Spores</size></color>\n\n" +
                    "<color=black>Adjacency bonus:</color> Explode on death\n\n" +
                    "Adjacent spawners spawn units that explode on death, dealing 25% of their maximum health as damage to surrounding enemies.";
                break;

            case 8:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "<size=30>Spell     <color=black>Cost:</color> 25</size>\n\n" +
                    "<color=black><size=30>Natural Recovery</size></color>\n\n" +
                    "<color=black>Spell cost:</color> 30\n" +
                    "<color=black>Cooldown:</color> 20\n" +
                    "<color=black>Effect:</color> 50% healing + resurrection\n" +
                    "<color=black>Adjacency bonus:</color> Unit health +20%\n\n" +
                    "Casts a spell that heals your units or resurrects them if their corpses still exist.";
                break;

            case 9:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "<color=black>Bonus:</color>\n\n" +
                    "This piece spawns units with 20% increased damage.";
                break;

            case 10:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "<color=black>Bonus:</color>\n\n"+
                    "This piece spawns units that explode on death, dealing 25% of their maximum health as damage to surrounding enemies.";
                break;

            case 11:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "<color=black>Bonus:</color>\n\n" +
                    "This piece spawns units with 20% increased maximum health.";
                break;

            case 12:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "<color=black>Bonus:</color>\n\n" +
                    "These pieces spawn units 20% faster.";
                break;

            case 13:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "<color=black>Bonus:</color>\n\n" +
                    "This spell is 25% more effective.";
                break;

            case 14:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "<color=black>Bonus:</color>\n\n" +
                    "This spell's cooldown is reduced by 20%.";
                break;

            case 15:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "<size=30>Bridge Control <color=black>Cost:</color> 50</size>\n\n" +
                    "<color=black>Effect:</color> Take control of the bridge\n\n" +
                    "Click to spawn a bridge of your color which only your units can use. If the opponent controls it, you take control. If you already control the bridge, it disappears for free. " +
                    "Units on top of the bridge fall down if the bridge disappears or becomes opposite color.";
                break;

            case 16:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "Activate all spawn pieces to resume spawning of units.\n\n" +
                    "You can also click an individual spawner to toggle it on/off.";
                break;

            case 17:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "Deactivate all spawn pieces to prevent units from spawning temporarily.\n\n" +
                    "You can also click an individual spawner to toggle it on/off.";
                break;

            case 18:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "Your current magic reserve. Used to build pieces and cast spells.\n\n" +
                    "Magic recharges if not used for a while.\n\n" +
                    "Normal maximum is 100.";
                break;

            case 19:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "Click these buttons or use keys <color=black><b>1, 2, 3</b></color> to change which lane all newly spawned units use.\n\n" +
                    "Units will not change lanes again after they have been assigned one.";
                break;
        }  
    }

	public void showTowerTooltip(){
		var temp = GameObject.FindGameObjectWithTag ("tooltip");
		temp.GetComponent<Image> ().enabled = true;
		switch (GetComponent<TowerPieceUI> ().builtType) {
		case TowerType.BlockType.Empty:
			temp.transform.GetChild (0).GetComponent<Text> ().text = "One of the places to build a piece of the tower.";
			break;

		case TowerType.BlockType.EmmettSpawn:
			temp.transform.GetChild (0).GetComponent<Text> ().text =
                "<color=black><size=30>Emmett</size></color>\n\n" +
                "             <size=30>Unit</size>\n\n" +
                "<color=black>Unit Value:</color> 5\n" +
                "<color=black>Spawn time:</color> 25\n" +
                "<color=black>Attack:</color> Melee, AoE\n" +
                "<color=black>Adjacency bonus:</color> Emmett spawn time -20%\n\n" +
                "Spawns Emmett, a powerful melee unit every 25 seconds.";
			break;

		case TowerType.BlockType.LamielSpawn:
			temp.transform.GetChild (0).GetComponent<Text> ().text =
                "<color=black><size=30>Lamiel</size></color>\n\n" +
                "             <size=30>Unit</size>\n\n" +
                "<color=black>Unit Value:</color> 1\n" +
                "<color=black>Spawn time:</color> 10\n" +
                "<color=black>Attack:</color> Ranged\n" +
                "<color=black>Adjacency bonus:</color> Lamiel spawn time -20%\n\n" +
                "Spawns Lamiel, a weak ranged unit every 10 seconds.";
			break;

		case TowerType.BlockType.SarkoSpawn:
			temp.transform.GetChild (0).GetComponent<Text> ().text =
                "<color=black><size=30>Sarko</size></color>\n\n" +
                "             <size=30>Unit</size>\n\n" +
                "<color=black>Unit Value:</color> 1\n" +
                "<color=black>Spawn time:</color> 10\n" +
                "<color=black>Attack:</color> Melee\n" +
                "<color=black>Special:</color> Rushes enemy tower at 50% health\n" +
                "<color=black>Adjacency bonus:</color> Sarko spawn time -20%\n\n" +
                "Spawns Sarko, a basic melee unit every 10 seconds.";
			break;

        case TowerType.BlockType.MaxMagic:
            temp.transform.GetChild(0).GetComponent<Text>().text =
                "<color=black><size=30>Brew Cellar</size></color>\n\n" +
                "             <size=30>Utility</size>\n\n" +
                "<color=black>Effect:</color> Max magic +100\n" +
                "<color=black>Adjacency bonus:</color> Spell cost -20%\n\n" +
                "Passively increases maximum magic by 100.";
            break;

        case TowerType.BlockType.DamageSpell:
            temp.transform.GetChild(0).GetComponent<Text>().text =
				"<color=black><size=28>Volatile Brew (hotkey: z)</size></color>\n\n" +
                "             <size=28>Spell</size>\n\n" +
                "<color=black>Spell cost:</color> 30\n" +
                "<color=black>Cooldown:</color> 20\n" +
                "<color=black>Effect:</color> Damage + knockback\n" +
                "<color=black>Adjacency bonus:</color> Unit dmg +20%\n\n" +
                "Casts a spell that deals damage to enemies and throws them around. Left click this and then an area to cast. Right click to cancel.";
            break;

        case TowerType.BlockType.MagicRegen:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "<color=black><size=30>Automated Brewery</size></color>\n\n" +
                    "             <size=30>Utility</size>\n\n" +
                    "<color=black>Effect:</color> Magic regen +50%\n" +
                    "<color=black>Adjacency bonus:</color> Spell effect +25%\n\n" +
                    "Passively increases magic regeneration by 50%.";
                break;

        case TowerType.BlockType.BoostSpell:
                temp.transform.GetChild(0).GetComponent<Text>().text =
					"<color=black><size=30>Minion Fertilizer (hotkey: z)</size></color>\n\n" +
                    "             <size=30>Spell</size>\n\n" +
                    "<color=black>Spell cost:</color> 30\n" +
                    "<color=black>Cooldown:</color> 20\n" +
                    "<color=black>Effect:</color> Unit stats +50%\n" +
                    "<color=black>Duration:</color> 15\n" +
                    "<color=black>Adjacency bonus:</color> Spell effect +25%\n\n" +
                    "Casts a spell that boosts the stats and size of your units. Left click this and then an area to cast. Right click to cancel.";
                break;

        case TowerType.BlockType.ExplodingUnits:
                temp.transform.GetChild(0).GetComponent<Text>().text =
                    "<color=black><size=30>Explosive Spores</size></color>\n\n" +
                    "             <size=30>Utility</size>\n\n" +
                    "<color=black>Adjacency bonus:</color> Explode on death\n\n" +
                    "Adjacent spawners spawn units that explode on death, dealing 25% of their maximum health as damage to surrounding enemies.";
                break;

        case TowerType.BlockType.HealSpell:
                temp.transform.GetChild(0).GetComponent<Text>().text =
					"<color=black><size=30>Natural Recovery (hotkey: x)</size></color>\n\n" +
                    "             <size=30>Spell</size>\n\n" +
                    "<color=black>Spell cost:</color> 30\n" +
                    "<color=black>Cooldown:</color> 20\n" +
                    "<color=black>Effect:</color> 50% healing + resurrection\n" +
                    "<color=black>Adjacency bonus:</color> Unit health +20%\n\n" +
                    "Casts a spell that heals your units or resurrects them if their corpses still exist. Left click this and then an area to cast. Right click to cancel.";
                break;
        }
	}

	public void hideTooltip(){
		var temp = GameObject.FindGameObjectWithTag ("tooltip");
		temp.GetComponent<Image> ().enabled = false;
		temp.transform.GetChild (0).GetComponent<Text> ().text = "";
	}

	public void setText(string text){
		GetComponent<Text> ().text = text;
	}
}
