﻿using UnityEngine;
using System.Collections;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class TowerHealth : MonoBehaviour {

    public float health = 10000;

    [PunRPC]
    public void TakeDamage(float amount)
    {
        health -= amount;

        if (health <= 0)
        {
            health = 0;
            Destroy(gameObject);
        }     
    }
}
