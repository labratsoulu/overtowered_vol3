﻿using UnityEngine;
using System.Collections;

public class PlacedConnectorPiece : MonoBehaviour {

	public LayerMask ignoreLayer;

	[PunRPC]
	public void setAllParents(int parentID, int siblingInd){
		gameObject.layer = ignoreLayer;
		GameObject towerBase;
		if (PhotonNetwork.player.ID == 1) {
			towerBase = GameObject.Find("Tower Base (Blue)");
		} else {
			towerBase = GameObject.Find ("Tower Base (Red)");
		}
		GameObject correctPiece = findCorrectPiece (parentID, towerBase);
		Debug.Log (correctPiece);
		transform.SetParent(correctPiece.transform.GetChild(siblingInd));
		transform.localRotation = Quaternion.Euler(Vector3.zero);
	}

	public GameObject findCorrectPiece(int IDNumber, GameObject towerBase){
		foreach (Transform child in towerBase.transform) {
			if (child.gameObject.GetComponent<PieceID> () != null && child.gameObject.GetComponent<PieceID> ().ID == IDNumber)
				return child.gameObject;
			else {
				GameObject result;
				result = findCorrectPiece (IDNumber, child.gameObject);
				if (result != null)
					return result;
			}
		}

		return null;
	}
}
