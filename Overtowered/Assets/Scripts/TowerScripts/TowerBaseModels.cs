﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBaseModels : MonoBehaviour {

	public GameObject takBase;
	public GameObject turbyBase;
    public int ownerID;

	void OnEnable()
    {
		if (PhotonNetwork.offlineMode == true)
        {
            if (ownerID == 1)
            {
                if (((int)PhotonNetwork.player.CustomProperties["selectedWizard"]) == 0)
                {
                    turbyBase.SetActive(true);
                }

                else // selectedWizard == 1
                {
                    takBase.SetActive(true);
                }
            }

            else
            {
                takBase.SetActive(true);
            }
        }

        else
        {
            if (((int)PhotonPlayer.Find(ownerID).CustomProperties["selectedWizard"]) == 0)
            {
                turbyBase.SetActive(true);
            }

            else // selectedWizard == 1
            {
                takBase.SetActive(true);
            }
        }       
	}
}
