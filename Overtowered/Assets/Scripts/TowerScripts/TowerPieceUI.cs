﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class TowerPieceUI : MonoBehaviour, IPointerClickHandler {

	public TowerPieceUI up;
	public TowerPieceUI down;
	public TowerPieceUI left;
	public TowerPieceUI right;

	public TowerBaseSpawner redTowerBase;
	public TowerBaseSpawner blueTowerBase;
	public GameObject redPiece;
	public GameObject bluePiece;

	public UIPieceState state;
    public bool isStartPiece;
	public Sprite activatedSprite;
	public Sprite builtSpriteEmmet;
	public Sprite builtSpriteSarko;
	public Sprite builtSpriteLamiel;
	public Sprite builtSpriteBoostSpell;
	public Sprite builtSpriteDamageSpell;
	public Sprite builtSpriteHealSpell;
	public Sprite builtSpriteMagicRegen;
	public Sprite builtSpriteMaxMagic;
	public Sprite builtSpriteExplodingUnits;
	public Sprite redActivated;
	public Sprite blueActivated;
	public Sprite redFrame;
	public Sprite blueFrame;

	public Image frameObject;

	public GameObject taksRadialMenu;
	public GameObject tipsysRadialMenu;
	public GameObject sellMenu;

	public PlayerVars player1Vars;
	public PlayerVars player2Vars;
	private PlayerVars totalMagic;
	public GameObject noMagicText;

	public Constants constants;

	public GameObject radialMenuPositioningObject;

	private Button button;

	private static Color blinkState;
	private static bool hasChanged;

	[HideInInspector]
	public TowerType.BlockType builtType;

	[HideInInspector]
	public float cooldownTimer;

	[HideInInspector]
	public float adjustedCooldownTime;

	[HideInInspector]
	public float cooldownTime;

	[HideInInspector]
	public bool active;

	public OpponentUI opponentsUI;

	[HideInInspector]
	public float unitHealthUp;
	[HideInInspector]
	public float unitDamageUp;
	[HideInInspector]
	public float spellEffectUp;
	[HideInInspector]
	public float spellCostDown;
	[HideInInspector]
	public int explodingUnits;

	public List<GameObject> boosts;
	public GameObject hpBoostSprite;
	public GameObject damageBoostSprite;
	public GameObject spellBoostSprite;
	public GameObject spawnBoostSprite;
	public GameObject spellCostSprite;
	public GameObject explodingUnitsSprite;
	private GameObject bonusSprite;

	private bool blinking;
	private float blinktimer;

    public enum UIPieceState
	{
		locked,
		activated,
		built
	};

	// Use this for initialization
	void Awake () {
		button = GetComponent<Button> ();
		blinkState = new Color (1, 1, 1, 1);
		boosts = new List<GameObject> ();
        builtType = TowerType.BlockType.Empty;
        active = true;
        cooldownTimer = 0;
        adjustedCooldownTime = 10;
        cooldownTime = 10;
        unitDamageUp = 1;
        unitHealthUp = 1;
        spellEffectUp = 1;
        spellCostDown = 1;
        explodingUnits = 0;

		if (state == UIPieceState.activated) {
			frameObject.enabled = true;
			if (PhotonNetwork.player.ID == 1) {
				GetComponent<Image> ().sprite = redActivated;
				frameObject.sprite = redFrame;
			} else {
				GetComponent<Image> ().sprite = blueActivated;
				frameObject.sprite = blueFrame;
			}
		}
    }
	
	// Update is called once per frame
	void Update () {
		if (state == UIPieceState.locked && ((up != null && up.state == UIPieceState.built) || (down != null && down.state == UIPieceState.built) || (left != null && left.state == UIPieceState.built) || (right != null && right.state == UIPieceState.built))) {
			state = UIPieceState.activated;
			button.enabled = true;
			frameObject.enabled = true;
			if (PhotonNetwork.player.ID == 1) {
				GetComponent<Image> ().sprite = redActivated;
				frameObject.sprite = redFrame;
			} else {
				GetComponent<Image> ().sprite = blueActivated;
				frameObject.sprite = blueFrame;
			}
		} else if (builtType == TowerType.BlockType.ApprenticeSpawn || builtType == TowerType.BlockType.GolemSpawn || builtType == TowerType.BlockType.SkeletonSpawn ||
		           builtType == TowerType.BlockType.EmmettSpawn || builtType == TowerType.BlockType.SarkoSpawn || builtType == TowerType.BlockType.LamielSpawn) {
			cooldownTimer += Time.deltaTime;
			float barSize;
			if ((cooldownTimer / adjustedCooldownTime) * 100 >= 100)
				barSize = 100;
			else
				barSize = (cooldownTimer / adjustedCooldownTime) * 100;
			transform.GetChild (0).GetComponent<Slider> ().value = barSize;
			if (cooldownTimer >= adjustedCooldownTime && active) {
				TowerBaseSpawner towerBase;
				//get relevant basepiece
				if (PhotonNetwork.player.ID == 1) {
					towerBase = redTowerBase;
				} else { // ID == 2
					towerBase = blueTowerBase;
				}
				if(explodingUnits > 0)
					towerBase.spawnUnitByType (builtType, unitDamageUp, unitHealthUp, true);
				else 
					towerBase.spawnUnitByType (builtType, unitDamageUp, unitHealthUp, false);
				cooldownTimer = 0;
			}
		} else if (builtType == TowerType.BlockType.BoostSpell || builtType == TowerType.BlockType.DamageSpell || builtType == TowerType.BlockType.HealSpell) {
			cooldownTimer += Time.deltaTime;
			float barSize;
			if ((cooldownTimer / adjustedCooldownTime) * 100 >= 100) {
				barSize = 100;
				if (blinktimer == 0)
					blinking = true;
			}else
				barSize = (cooldownTimer / adjustedCooldownTime) * 100;
			transform.GetChild (0).GetComponent<Slider> ().value = barSize;
		}
		hasChanged = false;

		if (blinking) {
			blinktimer += Time.deltaTime;
			if ((blinktimer > 0.1f && blinktimer < 0.2) || (blinktimer > 0.3f && blinktimer < 0.4) || (blinktimer > 0.5f && blinktimer < 0.6) || (blinktimer > 0.7f && blinktimer < 0.8) || (blinktimer > 0.9 && blinktimer < 1f)) {
				GetComponent<Image> ().enabled = false;
			} else if (blinktimer > 1f) {
				GetComponent<Image> ().enabled = true;
				blinking = false;
			} else {
				GetComponent<Image> ().enabled = true;
			}
		}

		//Adjencency bonus
		if (state == UIPieceState.built) {
			adjustedCooldownTime = cooldownTime;
			if (up != null && up.state == UIPieceState.built) {
				if ((builtType == TowerType.BlockType.ApprenticeSpawn || builtType == TowerType.BlockType.ApprenticeSpawn || builtType == TowerType.BlockType.ApprenticeSpawn ||
					builtType == TowerType.BlockType.EmmettSpawn || builtType == TowerType.BlockType.SarkoSpawn || builtType == TowerType.BlockType.LamielSpawn) && up.builtType == builtType) {
					adjustedCooldownTime *= 0.8f;
				}
			}
			if (down != null && down.state == UIPieceState.built) {
				if ((builtType == TowerType.BlockType.ApprenticeSpawn || builtType == TowerType.BlockType.ApprenticeSpawn || builtType == TowerType.BlockType.ApprenticeSpawn ||
					builtType == TowerType.BlockType.EmmettSpawn || builtType == TowerType.BlockType.SarkoSpawn || builtType == TowerType.BlockType.LamielSpawn) && down.builtType == builtType) {
					adjustedCooldownTime *= 0.8f;
				}
			}
			if (left != null && left.state == UIPieceState.built) {
				if ((builtType == TowerType.BlockType.ApprenticeSpawn || builtType == TowerType.BlockType.ApprenticeSpawn || builtType == TowerType.BlockType.ApprenticeSpawn ||
					builtType == TowerType.BlockType.EmmettSpawn || builtType == TowerType.BlockType.SarkoSpawn || builtType == TowerType.BlockType.LamielSpawn) && left.builtType == builtType) {
					adjustedCooldownTime *= 0.8f;
				}
			}
			if (right != null && right.state == UIPieceState.built) {
				if ((builtType == TowerType.BlockType.ApprenticeSpawn || builtType == TowerType.BlockType.ApprenticeSpawn || builtType == TowerType.BlockType.ApprenticeSpawn ||
					builtType == TowerType.BlockType.EmmettSpawn || builtType == TowerType.BlockType.SarkoSpawn || builtType == TowerType.BlockType.LamielSpawn) && right.builtType == builtType) {
					adjustedCooldownTime *= 0.8f;
				}
			}

			if (up != null && up.state == UIPieceState.built) {
				if ((builtType == TowerType.BlockType.BoostSpell || builtType == TowerType.BlockType.DamageSpell || builtType == TowerType.BlockType.HealSpell) && up.builtType == TowerType.BlockType.MaxMagic) {
					adjustedCooldownTime *= 0.8f;
				}
			}
			if (down != null && down.state == UIPieceState.built) {
				if ((builtType == TowerType.BlockType.BoostSpell || builtType == TowerType.BlockType.DamageSpell || builtType == TowerType.BlockType.HealSpell) && down.builtType == TowerType.BlockType.MaxMagic) {
					adjustedCooldownTime *= 0.8f;
				}
			}
			if (left != null && left.state == UIPieceState.built) {
				if ((builtType == TowerType.BlockType.BoostSpell || builtType == TowerType.BlockType.DamageSpell || builtType == TowerType.BlockType.HealSpell) && left.builtType == TowerType.BlockType.MaxMagic) {
					adjustedCooldownTime *= 0.8f;
				}
			}
			if (right != null && right.state == UIPieceState.built) {
				if ((builtType == TowerType.BlockType.BoostSpell || builtType == TowerType.BlockType.DamageSpell || builtType == TowerType.BlockType.HealSpell) && right.builtType == TowerType.BlockType.MaxMagic) {
					adjustedCooldownTime *= 0.8f;
				}
			}
		}
	}

	void LateUpdate(){
		if (state == UIPieceState.activated) {
			if (!hasChanged) {
				blinkState.r = blinkState.g = blinkState.b = blinkState.r - 0.4f * Time.deltaTime;
				if (blinkState.r <= 0.4f)
					blinkState.r = blinkState.b = blinkState.g = 1f;
			}
			GetComponent<Image> ().color = blinkState;
			hasChanged = true;
		}
	}

	public void pressed(){

		if (state == UIPieceState.activated) {
			GameObject temp;
			Debug.Log ((int)PhotonNetwork.player.CustomProperties ["selectedWizard"]);
			if ((int)PhotonNetwork.player.CustomProperties ["selectedWizard"] == 0)
				temp = Instantiate (tipsysRadialMenu);
			else 
				temp = Instantiate (taksRadialMenu); 

			temp.transform.SetParent (transform.parent.parent,false);

			if (radialMenuPositioningObject != null)
				temp.transform.position = radialMenuPositioningObject.transform.position;
			else
				temp.transform.position = transform.position;
			temp.GetComponent<RMenu> ().UIPiece = gameObject;

		} else if (state == UIPieceState.built) {
			if (builtType == TowerType.BlockType.ApprenticeSpawn || builtType == TowerType.BlockType.GolemSpawn || builtType == TowerType.BlockType.SkeletonSpawn ||
			    builtType == TowerType.BlockType.EmmettSpawn || builtType == TowerType.BlockType.SarkoSpawn || builtType == TowerType.BlockType.LamielSpawn) {
				toggle ();
			} else if (builtType == TowerType.BlockType.BoostSpell || builtType == TowerType.BlockType.DamageSpell || builtType == TowerType.BlockType.HealSpell) {
				if (transform.GetChild (0).GetComponent<Slider> ().value == 100) {
					SpellList spells = GameObject.FindGameObjectWithTag ("GameManager").GetComponent<SpellList> ();
					switch (builtType) {
					case TowerType.BlockType.BoostSpell:
						spells.boostSpell (this);
						break;

					case TowerType.BlockType.DamageSpell:
						spells.damageSpell (this);
						break;

					case TowerType.BlockType.HealSpell:
						spells.healSpell (this);
						break;
					}
				}
			}
		}
	}

	public void buildPiece(TowerType.BlockType type, int cost, float cooldown){
		cooldownTime = cooldown;
		adjustedCooldownTime = cooldown;

		if (PhotonNetwork.player.ID == 1)
		{
			totalMagic = player1Vars;
		}

		else // ID == 2
		{
			totalMagic = player2Vars;
		}
		if (totalMagic.magic >= cost) {

			totalMagic.calculateMagic (-cost);

			state = UIPieceState.built;

			GameObject buildPiece; 
			TowerBaseSpawner towerBase;

			//get relevant basepiece
			if (PhotonNetwork.player.ID == 1) {
				buildPiece = redPiece;
				towerBase = redTowerBase;
			} else { // ID == 2
				buildPiece = bluePiece;
				towerBase = blueTowerBase;
			}

			buildPiece.GetComponent<PhotonView> ().RPC ("showPiece", PhotonTargets.All, PhotonNetwork.player.ID);


			//needs to be replaced by the right sprite, based on the piece chosen
			switch (type) {
			case TowerType.BlockType.EmmettSpawn:
				GetComponent<Image> ().sprite = builtSpriteEmmet;
				opponentsUI.GetComponent<PhotonView> ().RPC ("informOpponent", PhotonTargets.Others, "emmet");
				bonusSprite = spawnBoostSprite;
				transform.GetChild (0).gameObject.SetActive (true);
				break;

			case TowerType.BlockType.LamielSpawn:
				GetComponent<Image> ().sprite = builtSpriteLamiel;
				opponentsUI.GetComponent<PhotonView> ().RPC ("informOpponent", PhotonTargets.Others, "lamiel");
				bonusSprite = spawnBoostSprite;
				transform.GetChild (0).gameObject.SetActive (true);
				break;

			case TowerType.BlockType.SarkoSpawn:
				GetComponent<Image> ().sprite = builtSpriteSarko;
				opponentsUI.GetComponent<PhotonView> ().RPC ("informOpponent", PhotonTargets.Others, "sarko");
				bonusSprite = spawnBoostSprite;
				transform.GetChild (0).gameObject.SetActive (true);
				break;

			case TowerType.BlockType.BoostSpell:
				cooldownTimer = cooldown;
				GetComponent<Image> ().sprite = builtSpriteBoostSpell;
				opponentsUI.GetComponent<PhotonView> ().RPC ("informOpponent", PhotonTargets.Others, "boostSpell");
				//GameObject.FindGameObjectWithTag ("GameManager").GetComponent<SpellList> ().addBoostSpellPiece (this);
				bonusSprite = spellBoostSprite;
				transform.GetChild (0).gameObject.SetActive (true);
				break;

			case TowerType.BlockType.DamageSpell:
				cooldownTimer = cooldown;
				GetComponent<Image> ().sprite = builtSpriteDamageSpell;
				opponentsUI.GetComponent<PhotonView> ().RPC ("informOpponent", PhotonTargets.Others, "damageSpell");
				//GameObject.FindGameObjectWithTag ("GameManager").GetComponent<SpellList> ().addDamageSpellPiece (this);
				bonusSprite = damageBoostSprite;
				transform.GetChild (0).gameObject.SetActive (true);
				break;

			case TowerType.BlockType.HealSpell:
				cooldownTimer = cooldown;
				GetComponent<Image> ().sprite = builtSpriteHealSpell;
				opponentsUI.GetComponent<PhotonView> ().RPC ("informOpponent", PhotonTargets.Others, "healSpell");
				//GameObject.FindGameObjectWithTag ("GameManager").GetComponent<SpellList> ().addHealSpellPiece (this);
				bonusSprite = hpBoostSprite;
				transform.GetChild (0).gameObject.SetActive (true);
				break;

			case TowerType.BlockType.MaxMagic:
				GetComponent<Image> ().sprite = builtSpriteMaxMagic;
				totalMagic.maxMagic += 100;
				opponentsUI.GetComponent<PhotonView> ().RPC ("informOpponent", PhotonTargets.Others, "maxMagic");
				bonusSprite = spellCostSprite;
				break;

			case TowerType.BlockType.MagicRegen:
				GetComponent<Image> ().sprite = builtSpriteMagicRegen;
				totalMagic.magicRechargeMultiplier += 0.5f;
				opponentsUI.GetComponent<PhotonView> ().RPC ("informOpponent", PhotonTargets.Others, "magicRegen");
				bonusSprite = spellBoostSprite;
				break;

			case TowerType.BlockType.ExplodingUnits:
				GetComponent<Image> ().sprite = builtSpriteExplodingUnits;
				opponentsUI.GetComponent<PhotonView> ().RPC ("informOpponent", PhotonTargets.Others, "explodingUnits");
				bonusSprite = explodingUnitsSprite;
				break;
			}
			GetComponent<Image> ().color = new Color (1, 1, 1, 1);

			builtType = type;

			if (up != null){
				setupAdjecencyBonus ("up");
				up.setupAdjecencyBonus ("down");
			}
			if (down != null) {
				setupAdjecencyBonus ("down");
				down.setupAdjecencyBonus ("up");
			}
			if (left != null) {
				setupAdjecencyBonus ("left");
				left.setupAdjecencyBonus ("right");
			}
			if (right != null) {
				setupAdjecencyBonus ("right");
				right.setupAdjecencyBonus ("left");
			}
		} else {
			noMagicText.SetActive (true);
			noMagicText.GetComponent<NoMagicText> ().showText ();
			GameObject.FindGameObjectWithTag ("narrator").GetComponent<NarratorSpeech> ().noMagic ();
		}
	}

	public void resetTimer(){
		cooldownTimer = 0f;
		blinktimer = 0f;
	}

	public void toggle(){
		//activate or deactivate piece
		if (active) {
			active = false;
			GetComponent<Image> ().color = new Color (0.5f, 0.5f, 0.5f, 1f);
		} else {
			active = true;
			GetComponent<Image> ().color = new Color (1f, 1f, 1f, 1f);
		}
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (eventData.button == PointerEventData.InputButton.Right && builtType != TowerType.BlockType.Empty) {
			GameObject temp = Instantiate (sellMenu); 
			temp.transform.SetParent (transform.parent.parent);
			temp.transform.position = transform.position;
			temp.GetComponent<SellMenu> ().piece = this;
		}
	}
		
	public void emptyRoom(){
		removeBonus ();
		if (builtType == TowerType.BlockType.MaxMagic) {
			totalMagic.maxMagic -= 100;
			if (totalMagic.magic > totalMagic.maxMagic)
				totalMagic.magic = totalMagic.maxMagic;
		} else if (builtType == TowerType.BlockType.MagicRegen) {
			totalMagic.magicRechargeMultiplier -= 0.5f;
		}

		builtType = TowerType.BlockType.Empty;
		state = UIPieceState.activated;
		if (PhotonNetwork.player.ID == 1) {
			GetComponent<Image> ().sprite = redActivated;
		} else {
			GetComponent<Image> ().sprite = blueActivated;
		}
		cooldownTimer = 0;
		adjustedCooldownTime = 10;
		cooldownTime = 10;
        active = true;
		transform.GetChild (0).gameObject.SetActive (false);
		opponentsUI.GetComponent<PhotonView> ().RPC ("informOpponent", PhotonTargets.Others, "Empty");
		bonusSprite = null;
		unitDamageUp = 1;
		unitHealthUp = 1;
		spellEffectUp = 1;
		spellCostDown = 1;
		explodingUnits = 0;
		while (boosts.Count > 0) {
			Destroy (boosts [0]);
			boosts.RemoveAt (0);
		}
	}

	public void setupAdjecencyBonus(string direction){
		if (builtType != TowerType.BlockType.Empty) {
			GameObject bonusSpriteObject = Instantiate (bonusSprite);
			switch (direction) {
			case "up":
				setAndShowBonus (up, bonusSpriteObject);
				break;
			case "down":
				bonusSpriteObject.transform.Rotate(0,0,180);
				setAndShowBonus (down, bonusSpriteObject);
				break;
			case "left":
				bonusSpriteObject.transform.Rotate(0,0,90);
				setAndShowBonus (left, bonusSpriteObject);
				break;
			case "right":
				bonusSpriteObject.transform.Rotate(0,0,-90);
				setAndShowBonus (right, bonusSpriteObject);
				break;
			}
		}
	}

	private void setAndShowBonus(TowerPieceUI piece, GameObject bonusSpriteObject, bool setBoost = true){
		if ((builtType == TowerType.BlockType.BoostSpell || builtType == TowerType.BlockType.MagicRegen) && (piece.builtType == TowerType.BlockType.BoostSpell || piece.builtType == TowerType.BlockType.DamageSpell || piece.builtType == TowerType.BlockType.HealSpell)) {
			piece.spellEffectUp += constants.boostPieceBonus;
			setIcon (piece, bonusSpriteObject);
		} else if (builtType == TowerType.BlockType.DamageSpell && (piece.builtType == TowerType.BlockType.EmmettSpawn || piece.builtType == TowerType.BlockType.SarkoSpawn || piece.builtType == TowerType.BlockType.LamielSpawn)) {
			piece.unitDamageUp += constants.damagePieceBonus;
			setIcon (piece, bonusSpriteObject);
		} else if (builtType == TowerType.BlockType.HealSpell && (piece.builtType == TowerType.BlockType.EmmettSpawn || piece.builtType == TowerType.BlockType.SarkoSpawn || piece.builtType == TowerType.BlockType.LamielSpawn)) {
			piece.unitHealthUp += constants.healPieceBonus;
			setIcon (piece, bonusSpriteObject);
		} else if ((builtType == TowerType.BlockType.EmmettSpawn || builtType == TowerType.BlockType.SarkoSpawn || builtType == TowerType.BlockType.LamielSpawn) && piece.builtType == builtType) {
			setIcon (piece, bonusSpriteObject);
		} else if (builtType == TowerType.BlockType.MaxMagic && (piece.builtType == TowerType.BlockType.BoostSpell || piece.builtType == TowerType.BlockType.DamageSpell || piece.builtType == TowerType.BlockType.HealSpell)) {
			//piece.spellCostDown -= constants.maxMagicPieceBonus;
			setIcon (piece, bonusSpriteObject);
		} else if (builtType == TowerType.BlockType.ExplodingUnits && (piece.builtType == TowerType.BlockType.EmmettSpawn || piece.builtType == TowerType.BlockType.SarkoSpawn || piece.builtType == TowerType.BlockType.LamielSpawn)) {
			piece.explodingUnits += 1;
			setIcon (piece, bonusSpriteObject);
		} else 
			Destroy(bonusSpriteObject);
	}

	private void setIcon(TowerPieceUI piece, GameObject bonusSpriteObject){
		Vector3 pos = piece.transform.position - transform.position;
		pos = pos / 2;
		pos = pos + transform.position;
		bonusSpriteObject.transform.SetParent (transform.parent.parent, false);
		bonusSpriteObject.transform.position = pos;
		piece.boosts.Add (bonusSpriteObject);
		boosts.Add (bonusSpriteObject);
	}

	public void removeBonus(){
		if (up != null && up.builtType != TowerType.BlockType.Empty) {
			removeBonus (up);
		}
		if (down != null && down.builtType != TowerType.BlockType.Empty) {
			removeBonus (down);
		}
		if (left != null && left.builtType != TowerType.BlockType.Empty) {
			removeBonus (left);
		}
		if (right != null && right.builtType != TowerType.BlockType.Empty) {
			removeBonus (right);
		}
	}

	private void removeBonus(TowerPieceUI piece){
		if (builtType == TowerType.BlockType.BoostSpell || builtType == TowerType.BlockType.MagicRegen) {
			piece.spellEffectUp -= constants.boostPieceBonus;
		} else if (builtType == TowerType.BlockType.DamageSpell) {
			piece.unitDamageUp -= constants.damagePieceBonus;
		} else if (builtType == TowerType.BlockType.HealSpell) {
			piece.unitHealthUp -= constants.healPieceBonus;
		} else if (builtType == TowerType.BlockType.MaxMagic) {
			//piece.spellCostDown += constants.maxMagicPieceBonus;
		} else if (builtType == TowerType.BlockType.ExplodingUnits) {
			piece.explodingUnits -= 1;
		}
	}
}
