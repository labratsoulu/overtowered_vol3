﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class UnitAI : MonoBehaviour {
   
    public GameObject initialTarget;
    public GameObject currentTarget;
    public LayerMask targetLayer;

    public enum unitTypes { Golem, Apprentice, Skeleton, Emmett, Sarko, Lamiel };
    public unitTypes unitType;

    public float health;
    public float damage;
    public float attackInterval;
    public float movementSpeed;
    public float attackDistance;
    public float magicReward;
    public float unitValue;
    public float maxHealth;
    public float deathTime;
    public float explosionRadius;

    public bool explodeOnDeath = false;
    public bool isAirborne = false;
    public float targetCheckInterval;
    public float targetAcquireDistance;

    public Collider[] enemyColliders;

    // Enum to check current unit state
    public enum unitStates { Moving, Attacking, Dead };
    public unitStates unitState;

    public GameObject projectile;
    public GameObject[] projectileStartLocations;
    public AreaAttack areaAttack;
    public GameObject[] unitEffects;
    public ParticleSystem deathExplosion;
    public ParticleSystem towerAttackEffect;
    public SkinnedMeshRenderer meshRenderer;

    private Animator anim;
    private PhotonView thisView;
    private NavMeshAgent agent;
    private Constants constants;
    private GameObject player1Vars;
    private GameObject player2Vars;    
    private LayerMask deadLayer;
    private LayerMask initialLayer;
    private float currentDistance;
    private bool isStunned = false;
    private float stunDuration = 0;
    private bool isDying = false;
    private int initialObstaclePriority;
    private bool isRushing = false;

    void Awake()
    {
        constants = GameObject.FindWithTag("Constants").GetComponent<Constants>();
        player1Vars = GameObject.FindWithTag("Player1");
        player2Vars = GameObject.FindWithTag("Player2");       
        agent = GetComponent<NavMeshAgent>();
        initialLayer = gameObject.layer;

        if (gameObject.layer == 9) // = Player1
        {
            gameObject.transform.parent = GameObject.FindWithTag("Player1Units").transform;
            initialTarget = GameObject.FindWithTag("Player2");
            deadLayer = LayerMask.NameToLayer("Player1Dead");
        }

        else if (gameObject.layer == 10) // = Player2
        {
            gameObject.transform.parent = GameObject.FindWithTag("Player2Units").transform;
            initialTarget = GameObject.FindWithTag("Player1");
            deadLayer = LayerMask.NameToLayer("Player2Dead");
        }

        switch (unitType)
        {
		    case unitTypes.Golem:
				health = constants.golemHealth;
				damage = constants.golemDamage;
				attackInterval = constants.golemAttackInterval;
				movementSpeed = constants.golemMovementSpeed;
				attackDistance = constants.golemAttackDistance;
				magicReward = constants.golemMagicReward;
                unitValue = constants.golemValue;
                break;

            case unitTypes.Apprentice:
                health = constants.apprenticeHealth;
                damage = constants.apprenticeDamage;
                attackInterval = constants.apprenticeAttackInterval;
                movementSpeed = constants.apprenticeMovementSpeed;
                attackDistance = constants.apprenticeAttackDistance;
                magicReward = constants.apprenticeMagicReward;
                unitValue = constants.apprenticeValue;
                break;

            case unitTypes.Skeleton:
                health = constants.skeletonHealth;
                damage = constants.skeletonDamage;
                attackInterval = constants.skeletonAttackInterval;
                movementSpeed = constants.skeletonMovementSpeed;
                attackDistance = constants.skeletonAttackDistance;
                magicReward = constants.skeletonMagicReward;
                unitValue = constants.skeletonValue;
                break;

            case unitTypes.Emmett:
                health = constants.emmettHealth;
                damage = constants.emmettDamage;
                attackInterval = constants.emmettAttackInterval;
                movementSpeed = constants.emmettMovementSpeed;
                attackDistance = constants.emmettAttackDistance;
                magicReward = constants.emmettMagicReward;
                unitValue = constants.emmettValue;
                break;

            case unitTypes.Sarko:
                health = constants.sarkoHealth;
                damage = constants.sarkoDamage;
                attackInterval = constants.sarkoAttackInterval;
                movementSpeed = constants.sarkoMovementSpeed;
                attackDistance = constants.sarkoAttackDistance;
                magicReward = constants.sarkoMagicReward;
                unitValue = constants.sarkoValue;
                break;

            case unitTypes.Lamiel:
                health = constants.lamielHealth;
                damage = constants.lamielDamage;
                attackInterval = constants.lamielAttackInterval;
                movementSpeed = constants.lamielMovementSpeed;
                attackDistance = constants.lamielAttackDistance;
                magicReward = constants.lamielMagicReward;
                unitValue = constants.lamielValue;
                break;
        }

        targetCheckInterval = constants.targetCheckInterval;
        targetAcquireDistance = constants.targetAcquireDistance;
        deathTime = constants.deathTime;
        explosionRadius = constants.unitExplosionRadius;
        maxHealth = health;
    }

    void Start()
    {
        thisView = GetComponent<PhotonView>();
        agent.stoppingDistance = attackDistance;
        agent.speed = movementSpeed;
        anim = GetComponent<Animator>();

        initialObstaclePriority = agent.avoidancePriority;

        // Start coroutine for checking targets
        StartCoroutine("SelectTarget");
    }

    void Update()
    {
        print(agent.areaMask);
        if (currentTarget == initialTarget)
        {
            agent.stoppingDistance = 10f;
        }

        else
        {
            agent.stoppingDistance = attackDistance;
        }

        //print("rem. dist: " + agent.remainingDistance);
        //print("pending: " + agent.pathPending);
        //print("pathstatus: " + agent.pathStatus);
        //print("haspath:" + agent.hasPath);
        anim.SetFloat("movementSpeedMultiplier", agent.speed / 4);
        anim.SetFloat("attackSpeedMultiplier", 1 / attackInterval);
        agent.avoidancePriority = initialObstaclePriority;

        if (isStunned)
        {
            agent.avoidancePriority = 4;

            if (stunDuration > 0)
            {
                stunDuration -= Time.deltaTime;
            }

            else
            {
                stunDuration = 0;

                if (!isAirborne)
                {
                    UnStun();
                }               
            }            
        }

        else
        {
            // Check if we have arrived at our target. If true, stop moving and start attacking
            if (agent.enabled)
            {
                float distanceToTower = Mathf.FloorToInt(Vector3.Distance(transform.position, initialTarget.transform.position));

                if (distanceToTower <= 12)
                {
                    StartCoroutine("DoTowerDamage");
                }

                if (currentTarget != null)
                {
                    currentDistance = Mathf.FloorToInt(Vector3.Distance(transform.position, currentTarget.transform.position));
                    //print(currentDistance);

                    if (!agent.pathPending && currentDistance <= agent.stoppingDistance)
                    {
                        anim.SetBool("isMoving", false);
                        anim.SetBool("isAttacking", true);
                        unitState = unitStates.Attacking;
                    }

                    // Otherwise, keep moving
                    else
                    {
                        anim.SetBool("isMoving", true);
                        anim.SetBool("isAttacking", false);
                        unitState = unitStates.Moving;
                    }
                }                
            }

            // Smooth facing towards the target if attacking
            if (unitState == unitStates.Attacking)
            {
                if (currentTarget != null)
                {
                    Vector3 targetPos = currentTarget.transform.position - transform.position;
                    Quaternion newRot = Quaternion.LookRotation(targetPos);
                    transform.rotation = Quaternion.Lerp(transform.rotation, newRot, Time.deltaTime * 4);
                    agent.avoidancePriority = 4;
                }
            }
        }        
    }

    private void OnCollisionEnter(Collision col)
    {
        if (isAirborne && stunDuration == 0)
        {
            if (col.gameObject.tag == "Ground")
            {
                UnStun();
            }
        }
        
        if (col.gameObject.tag == "KillZone")
        {
            unitState = unitStates.Dead;
            DestroyUnit();
        }   
    }

    private void OnCollisionStay(Collision col)
    {
        if (isAirborne && stunDuration == 0)
        {
            if (col.gameObject.tag == "Ground")
            {
                UnStun();
            }
        }
    }

    // Function to select target. The target needs to be in <targetLayer>, otherwise ignored
    private IEnumerator SelectTarget()
    {   
        while (true)
        {
            // Default to Tower if nothing on target
            if (currentTarget == null)
            {
                currentTarget = initialTarget;
            }

            // If currently targeting Tower, start searching enemy units nearby and if there are, set closest as a potential target
            if (currentTarget = initialTarget)
            {
                enemyColliders = Physics.OverlapBox(transform.position, new Vector3(targetAcquireDistance, 5, targetAcquireDistance), Quaternion.identity, targetLayer);

                if (enemyColliders.Length != 0)
                {
                    // This should sort the array in order from the closest to the furthest one
                    System.Array.Sort(enemyColliders, delegate (Collider a, Collider b) {
                        return (transform.position - a.transform.position).sqrMagnitude.CompareTo((transform.position - b.transform.position).sqrMagnitude);
                    });

                    currentTarget = enemyColliders[0].gameObject;
                }   
            }
            
            // Currently targeting a unit, check if it's dead to move on
            else
            {
                if (currentTarget.GetComponent<UnitAI>().unitState == unitStates.Dead)
                {
                    currentTarget = initialTarget;
                }
            }
           
            agent.destination = currentTarget.transform.position;
            yield return new WaitForSeconds(targetCheckInterval);
        }
    }

    private void UnStun()
    {
        anim.enabled = true;
        isStunned = false;

        if (agent.enabled == false && unitState != unitStates.Dead)
        {
            agent.enabled = true;
        }

        agent.isStopped = false;

        if (GetComponent<Rigidbody>() != null)
        {
            Destroy(GetComponent<Rigidbody>());
        }

        if (!isRushing)
        {
            StartCoroutine("SelectTarget");
        }

        else
        {
            currentTarget = initialTarget;
            agent.destination = currentTarget.transform.position;
        }

        isAirborne = false;
    }

    // Function to calculate path length
    private static float GetPathLength(NavMeshPath path)
    {
        float lng = 0.0f;

        if ((path.status != NavMeshPathStatus.PathInvalid))
        {
            for (int i = 1; i < path.corners.Length; ++i)
            {
                lng += Vector3.Distance(path.corners[i - 1], path.corners[i]);
            }
        }

        return lng;
    }

    // Debug code to draw current path, target select area and attack range when unit is selected in editor
    private void OnDrawGizmosSelected()
    {
        // Red line for path
        for (int i = 0; i < agent.path.corners.Length - 1; i++)
        {
            Debug.DrawLine(agent.path.corners[i], agent.path.corners[i + 1], Color.red);
        }

        // Blue wirecube for target check area
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(transform.position, new Vector3(targetAcquireDistance * 2f, 10f, targetAcquireDistance * 2f));

        // Red sphere for attack distance
        Gizmos.color = new Color(0f, 1f, 0f, 0.5f);
        Gizmos.DrawSphere(transform.position, attackDistance);

        // Yellow sphere for death explosion radius
        if (explodeOnDeath)
        {
            Gizmos.color = new Color(1f, 0.92f, 0.016f, 0.35f);
            Gizmos.DrawSphere(transform.position, explosionRadius);
        }
    }

    // This is called in normal melee attack animation and projectile hit.
    public void DoDamage()
    {
        // We don't want units do deal damage to tower with attacks anymore, this checks for it.
        if (currentTarget != initialTarget)
        {
            if (currentTarget != null && currentTarget.activeSelf && PhotonNetwork.isMasterClient)
            {
                currentTarget.GetComponent<PhotonView>().RPC("TakeDamage", PhotonTargets.All, damage);
            }

            if (GetComponent<UnitSounds>() != null)
            {
                GetComponent<UnitSounds>().HitSound(currentTarget);
            }
        }       
    }

    // This is called in melee area attack animation.
    public void DoAreaAttack()
    {
        areaAttack.Attack();
    }

    // Used when unit reaches enemy tower
    public IEnumerator DoTowerDamage()
    {
        if (initialTarget != null && initialTarget.activeSelf && PhotonNetwork.isMasterClient)
        {
            initialTarget.GetComponent<PhotonView>().RPC("TakeDamage", PhotonTargets.All, unitValue);
        }

        if (GetComponent<UnitSounds>() != null)
        {
            GetComponent<UnitSounds>().SacrificeSound();
        }

        agent.enabled = false;
        meshRenderer.enabled = false;
        StopCoroutine("SelectTarget");
        unitState = unitStates.Dead;
        gameObject.layer = 0;
        towerAttackEffect.Play();
        yield return new WaitForSeconds(towerAttackEffect.main.duration);
        DestroyUnit();
    }

    // This is called in ranged attack animation.
    public void ShootProjectile(int location)
    {
        if (currentTarget != null && !agent.pathPending && currentDistance <= attackDistance)
        {
            GameObject proj = Instantiate(projectile, projectileStartLocations[location].transform.position, transform.rotation, gameObject.transform);
            proj.SetActive(true);
            proj.GetComponent<Projectile>().damage = damage;
            proj.GetComponent<Projectile>().target = currentTarget;
            proj.GetComponent<Projectile>().locationNumber = location;
        }     
    }

    [PunRPC]
    public void TakeDamage(float amount)
    {       
        health -= amount;

        if (health <= 0.5f * maxHealth && unitType == unitTypes.Sarko && !isRushing)
        {
            RushTower();
        }

        if (health <= 0)
        {
            // This option can be toggled on/off in constants.
            if (constants.giveMagicOnKill)
            {
                // Give magic to the killer (NOT the owner of this unit)
                if (!thisView.isMine)
                {
                    if (thisView.owner.ID == 1)
                    {
                        player2Vars.GetComponent<PhotonView>().RPC("calculateMagic", PhotonTargets.All, magicReward);
                    }
                    else // ID == 2
                    {
                        player1Vars.GetComponent<PhotonView>().RPC("calculateMagic", PhotonTargets.All, magicReward);
                    }
                }
            }

            health = 0;
            agent.enabled = false;
            anim.enabled = true;
            StopCoroutine("SelectTarget");
            anim.SetBool("isMoving", false);
            anim.SetBool("isAttacking", false);
            anim.SetBool("isDead", true);
            unitState = unitStates.Dead;
            gameObject.layer = deadLayer;

            foreach (GameObject effect in unitEffects)
            {
                effect.SetActive(false);
            }

            if (GetComponent<UnitSounds>() != null)
            {
                GetComponent<UnitSounds>().DeathSound();
            }

            if (explodeOnDeath)
            {
                Explode();
            }
        }   
    }

    private void RushTower()
    {
        isRushing = true;
        StopCoroutine("SelectTarget");
        currentTarget = initialTarget;
        agent.destination = currentTarget.transform.position;
        agent.speed = movementSpeed * 1.5f;
        agent.avoidancePriority = 0;
        agent.obstacleAvoidanceType = ObstacleAvoidanceType.NoObstacleAvoidance;
        anim.SetBool("isMoving", true);
        anim.SetBool("isAttacking", false);
        unitState = unitStates.Moving;
    }

    private void Explode()
    {
        Collider[] unitColliders = Physics.OverlapSphere(transform.position, explosionRadius, targetLayer);
        foreach (Collider unit in unitColliders)
        {
            unit.gameObject.GetComponent<UnitAI>().TakeDamage(maxHealth * 0.25f);
        }

        if (GetComponent<UnitSounds>() != null)
        {
            GetComponent<UnitSounds>().SporeExplosion();
        }

        deathExplosion.Play();
    }

    private void DecreaseUnitAmount()
    {
        if (!constants.testMode && !constants.offlineMode)
        {
            // Decrease unit count for the correct player
            if (thisView.isMine && !isDying)
            {
                isDying = true;

                if (thisView.owner.ID == 1)
                {
                    player1Vars.GetComponent<PlayerVars>().changeUnitAmount(-(int)unitValue);
                }
                else // ID == 2
                {
                    player2Vars.GetComponent<PlayerVars>().changeUnitAmount(-(int)unitValue);
                }
            }
        }
    }

    // Called at the end of death animation
    public void StartDestroy()
    {
        StartCoroutine("StartDestroyTimer");  
    }

    private IEnumerator StartDestroyTimer()
    {
        yield return new WaitForSeconds(deathTime);
        DestroyUnit();
    }

    public void DestroyUnit()
    {
        DecreaseUnitAmount();

        if (thisView.isMine && (unitState == unitStates.Dead || gameObject.layer == 0))
        {
            PhotonNetwork.Destroy(gameObject);
        }
    }

	// Heals unit, but never lets health go beyond initial max
    [PunRPC]
	public void HealPercentage(float amount)
	{
        if (gameObject.layer == deadLayer)
        {
            StopCoroutine("StartDestroyTimer");
            agent.enabled = true;
            anim.enabled = true;
            StartCoroutine("SelectTarget");
            anim.SetBool("isDead", false);
            anim.SetBool("isMoving", true);
            gameObject.layer = initialLayer;
        }

		if (health + (amount * maxHealth) >= maxHealth)
        {
            health = maxHealth;
        }

		else
        {
            health += (amount * maxHealth);
        }
	}

	// Stun this unit for a given amount of seconds
	public void Stun(float seconds)
    {
        isStunned = true;
		stunDuration = seconds;

        if (agent.enabled == true)
        {
            agent.isStopped = true;
        }

        anim.enabled = false;
        StopCoroutine("SelectTarget");
    }

    // Activate spell particle effect for all players
    [PunRPC]
    public void ActivateEffect(int effectNumber)
    {
        unitEffects[effectNumber].SetActive(true);
    }

    [PunRPC] 
    public void SyncValues(float damageMultiplier, float healthMultiplier, bool exploding)
    {
        damage *= damageMultiplier;
        maxHealth *= healthMultiplier;
        health *= healthMultiplier;
        explodeOnDeath = exploding;
    }
}
