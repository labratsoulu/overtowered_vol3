﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class UnitExploder : MonoBehaviour {

    public Constants constants;
    private LayerMask enemyLayer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    [PunRPC]
    public void ExplodeAtPoint(Vector3 position, int playerID, float damage)
    {
        if (playerID == 1)
        {
            enemyLayer = LayerMask.NameToLayer("Player2");
        }
        else // playerID == 2
        {
            enemyLayer = LayerMask.NameToLayer("Player1");
        }

        Collider[] enemyColliders = Physics.OverlapSphere(position, constants.spellRadius);

        foreach (Collider unit in enemyColliders)
        {
            if (unit.gameObject.layer == enemyLayer)
            {
                UnitAI unitAI = unit.gameObject.GetComponent<UnitAI>();
                unitAI.TakeDamage(damage);

                if (unitAI.unitState != UnitAI.unitStates.Dead)
                {
                    unit.GetComponent<NavMeshAgent>().enabled = false;
                    unitAI.Stun(constants.damageSpellStunDuration);
                    unitAI.isAirborne = true;
                    unit.gameObject.AddComponent<Rigidbody>();
                    unit.GetComponent<Rigidbody>().AddExplosionForce(damage / 2, position, constants.spellRadius, 5f, ForceMode.Impulse);
                }              
            }
        }
    }
    
}
