﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitHealthBar : MonoBehaviour {

	//toggle using TAB
	//have it update to correct size
	//take into account max health

	private UnitAI unitAI;
	private float maxHealth;
	private SpriteRenderer sprite;
    private Camera cam;

	void Awake () {
		sprite = GetComponent<SpriteRenderer> ();
		unitAI = transform.parent.GetComponent<UnitAI> ();
		maxHealth = unitAI.health;

        if (PhotonNetwork.player.ID == 1)
        {
            cam = GameObject.FindWithTag("Player1Camera").GetComponent<Camera>();
        }

        else
        {
            cam = GameObject.FindWithTag("Player2Camera").GetComponent<Camera>();
        }
	}

	void Update(){
		if (GameManager.healthBarOn) {
			if (!sprite.enabled)
				sprite.enabled = true;
			Vector3 scale = transform.localScale;
			scale.x = (unitAI.health / maxHealth) / 2;
			transform.localScale = scale;
		} else if(sprite.enabled) {
			sprite.enabled = false;
		}
	}

	void LateUpdate () {
		transform.LookAt(transform.position + cam.transform.rotation * Vector3.forward,
			cam.transform.rotation * Vector3.up);
	}
}
