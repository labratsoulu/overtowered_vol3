﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class UnitPath : MonoBehaviour {

    private NavMeshAgent agent;
    private PathToggler pathToggler;
    private PathSwitcher pathSwitcher;
    private PhotonView unitView;

    void Awake()
    {
        unitView = gameObject.GetComponent<PhotonView>();
        agent = GetComponent<NavMeshAgent>();
        agent.areaMask = 1;

        // Level 1 Bridge Toggle stuff
        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            pathToggler = GameObject.FindWithTag("PathManager").GetComponent<PathToggler>();

            if (gameObject.layer == 9) // = Player1
            {
                if (pathToggler.controllerID == 1)
                {
                    agent.areaMask = 9;

                    foreach (Material mat in GetComponentInChildren<SkinnedMeshRenderer>().materials)
                    {
                        mat.renderQueue = 2452;
                    }
                }
            }

            else if (gameObject.layer == 10) // = Player2
            {
                if (pathToggler.controllerID == 2)
                {
                    agent.areaMask = 9;

                    foreach (Material mat in GetComponentInChildren<SkinnedMeshRenderer>().materials)
                    {
                        mat.renderQueue = 2452;
                    }
                }
            }
        }

        // Level 2 Path Switch stuff
        else
        {
            pathSwitcher = GameObject.FindWithTag("PathManager").GetComponent<PathSwitcher>();

            if (pathSwitcher.selectedLane == 1)
            {
                unitView.RPC("SetUnitMask", PhotonTargets.All, 17);
            }

            else if (pathSwitcher.selectedLane == 2)
            {
                unitView.RPC("SetUnitMask", PhotonTargets.All, 33);
            }

            else if (pathSwitcher.selectedLane == 3)
            {
                unitView.RPC("SetUnitMask", PhotonTargets.All, 65);
            }
        }      
    }

    [PunRPC]
    private void SetUnitMask(int mask)
    {
        agent.areaMask = mask;
    }
}
