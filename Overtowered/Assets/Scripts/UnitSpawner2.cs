﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class UnitSpawner2 : MonoBehaviour {

	public GameObject redBase;
	public GameObject blueBase;
    public float spawnRadius = 1.0f;
    public Constants constants;

    void Start ()
    {
        spawnRadius = constants.unitSpawnRadius;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.F1))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    public void SpawnUnit(GameObject unit)
    {
        //for (int i = 0; i < 100; i++)
        //{
            PhotonNetwork.Instantiate(unit.name, new Vector3(transform.position.x + Random.Range(-spawnRadius, spawnRadius), transform.position.y, transform.position.z + Random.Range(-spawnRadius, spawnRadius)), transform.rotation, 0);
        //}
    }
}
