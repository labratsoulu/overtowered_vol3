﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class UnitStatChanger : MonoBehaviour {

	[HideInInspector]
	public float speedUpDuration = 0;
	[HideInInspector]
	public float damageUpDuration = 0;
	[HideInInspector]
	public float rangeUpDuration = 0;
	[HideInInspector]
	public float cDUpDuration = 0;
	[HideInInspector]
	public float speedDownDuration = 0;
	[HideInInspector]
	public float damageDownDuration = 0;
	[HideInInspector]
	public float rangeDownDuration = 0;
	[HideInInspector]
	public float cDDownDuration = 0;

	[HideInInspector]
	public float boostDuration = 0;

    public GameObject areaAttack;

	private Constants constants;
	private UnitAI ai;
    private NavMeshAgent agent;
	private float originalDamage;
	private float originalAttackSpeed;
	private float originalSpeed;
    private float originalAttackDistance;
    private Vector3 originalScale;
    private float originalExplosionRadius;
    private float originalAreaAttackRadius;
    private Vector3 originalAreaScale;

	// Use this for initialization
	void Start()
    {
		ai = GetComponent<UnitAI>();
        agent = GetComponent<NavMeshAgent>();
		constants = GameObject.FindGameObjectWithTag ("Constants").GetComponent<Constants> ();      
	}
	
	// Update is called once per frame
	void Update()
    {
		if (boostDuration > 0)
        {
			boostDuration -= Time.deltaTime;

			if (boostDuration <= 0)
            {
				agent.speed = originalSpeed;
				ai.damage = originalDamage;
				ai.attackInterval = originalAttackSpeed;
                ai.attackDistance = originalAttackDistance;
                ai.gameObject.transform.localScale = originalScale;
                ai.explosionRadius = originalExplosionRadius;

                if (areaAttack != null)
                {
                    areaAttack.GetComponent<AreaAttack>().attackRadius = originalAreaAttackRadius;
                    areaAttack.transform.GetChild(0).transform.localScale = originalAreaScale;
                }
            }
		}
//		if (speedUpDuration > 0) {
//			speedUpDuration -= Time.deltaTime;
//			if (speedUpDuration <= 0) {
//				GetComponent<UnityEngine.AI.NavMeshAgent>().speed -= constants.spellOfHodosChange;
//			}


	}

//	[PunRPC]
//	public void changeSpeed(bool up)
//	{
//		if (up)
//		{
//			GetComponent<UnityEngine.AI.NavMeshAgent>().speed += constants.spellOfHodosChange;
//			speedUpDuration = constants.buffDuration;
//		}
//		else
//		{
//			GetComponent<UnityEngine.AI.NavMeshAgent>().speed -= constants.spellOfHodosChange;
//			speedDownDuration = constants.buffDuration;
//		}
//	}

	[PunRPC]
	public void boostUnit(float extraBoost)
    {
		if (boostDuration <= 0)
        {
			boostDuration = constants.boostSpellDuration;

			originalSpeed = agent.speed;
			originalDamage = ai.damage;
			originalAttackSpeed = ai.attackInterval;
            originalAttackDistance = ai.attackDistance;
            originalScale = ai.gameObject.transform.localScale;
            originalExplosionRadius = ai.explosionRadius;

            if (areaAttack != null)
            {
                originalAreaAttackRadius = areaAttack.GetComponent<AreaAttack>().attackRadius;
                originalAreaScale = areaAttack.transform.GetChild(0).transform.localScale;
            }

            agent.speed *= (constants.boostSpellBoost * extraBoost);
            ai.damage *= (constants.boostSpellBoost * extraBoost);
            ai.attackInterval /= (constants.boostSpellBoost * extraBoost);						
            ai.attackDistance *= (constants.boostSpellBoost * extraBoost);
            ai.gameObject.transform.localScale *= (constants.boostSpellBoost * extraBoost);
            ai.explosionRadius *= (constants.boostSpellBoost * extraBoost);

            if (areaAttack != null)
            {
                areaAttack.GetComponent<AreaAttack>().attackRadius *= (constants.boostSpellBoost * extraBoost);
                areaAttack.transform.GetChild(0).transform.localScale *= (constants.boostSpellBoost * extraBoost);
            }           

        }

        else
        {
			boostDuration = constants.boostSpellDuration;
		}
	}
}
