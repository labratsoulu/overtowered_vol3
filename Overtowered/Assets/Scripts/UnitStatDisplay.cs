﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UnitStatDisplay : MonoBehaviour {

	protected RaycastHit hit;

	private Text textField;
	private UnitAI selectedUnit;

	// Use this for initialization
	void Start () {
		textField = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast (ray, out hit)) {
				selectedUnit = hit.transform.gameObject.GetComponent<UnitAI> ();
				if (selectedUnit == null)
					textField.text = "";
				else {
					if (selectedUnit.unitType == UnitAI.unitTypes.Golem)
						GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<UnitSpeech> ().playGolem();
					else if (selectedUnit.unitType == UnitAI.unitTypes.Apprentice)
						GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<UnitSpeech> ().playCultist();
					else if (selectedUnit.unitType == UnitAI.unitTypes.Skeleton)
						GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<UnitSpeech> ().playSkeleton();
				}
			}
		}
		if (selectedUnit != null) {
			textField.text = "Name:\t\t\t\t\t" + selectedUnit.unitType;
			textField.text += "\nHealth:\t\t\t\t\t" + selectedUnit.health;
			textField.text += "\nPower:\t\t\t\t\t" + selectedUnit.damage;
			if (selectedUnit.gameObject.GetComponent<UnitStatChanger> ().damageUpDuration > 0) 
				textField.text += " +10";
			if (selectedUnit.gameObject.GetComponent<UnitStatChanger> ().damageDownDuration > 0) 
				textField.text += " -10";

			textField.text += "\nAtk. Cooldown:\t" + selectedUnit.attackInterval;
			if (selectedUnit.gameObject.GetComponent<UnitStatChanger> ().cDUpDuration > 0) 
				textField.text += " -0.5";
			if (selectedUnit.gameObject.GetComponent<UnitStatChanger> ().cDDownDuration > 0) 
				textField.text += " +0.5";

			textField.text += "\nMove Speed:\t\t" + selectedUnit.movementSpeed;
			if (selectedUnit.gameObject.GetComponent<UnitStatChanger> ().speedUpDuration > 0) 
				textField.text += " +0.5";
			if (selectedUnit.gameObject.GetComponent<UnitStatChanger> ().speedDownDuration > 0) 
				textField.text += " -0.5";
			
			textField.text += "\nAtk. Range:\t\t" + selectedUnit.attackDistance;
			if (selectedUnit.gameObject.GetComponent<UnitStatChanger> ().rangeUpDuration > 0) 
				textField.text += " +5";
			if (selectedUnit.gameObject.GetComponent<UnitStatChanger> ().rangeDownDuration > 0) 
				textField.text += " -5";
		}
	}
}
