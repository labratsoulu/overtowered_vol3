﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTS_Cam;

public class WizardAnimator : MonoBehaviour
{
    private PhotonView thisView;
    private GameObject selectedWizard;
    private Animator animator;

    public GameObject[] wizards;

    //Toggled components
    public MonoBehaviour[] scriptsToEnable;
    public Camera cam;
    public GUILayer guiLayer;
    public FlareLayer flareLayer;

    void Awake()
    {
        thisView = GetComponent<PhotonView>();
        selectedWizard = wizards[(int)PhotonPlayer.Find(thisView.ownerId).CustomProperties["selectedWizard"]];
        selectedWizard.SetActive(true);
        animator = selectedWizard.GetComponent<Animator>();

        if (thisView.isMine)
        {
            cam.enabled = true;
            guiLayer.enabled = true;
            flareLayer.enabled = true;

            foreach (MonoBehaviour script in scriptsToEnable)
            {
                script.enabled = true;
            }

            // Set own wizard invisible to the camera
            if (PhotonNetwork.player.ID == 1)
            {
                gameObject.tag = "Player1Camera";
                cam.cullingMask = ~(1 << 13);

                foreach (Transform trans in selectedWizard.GetComponentsInChildren<Transform>())
                {
                    trans.gameObject.layer = 13;
                }
            }

            else // ID == 2 
            {
                gameObject.tag = "Player2Camera";
                cam.cullingMask = ~(1 << 14);

                foreach (Transform trans in selectedWizard.GetComponentsInChildren<Transform>())
                {
                    trans.gameObject.layer = 14;
                }
            }
        }     
    }

    [PunRPC]
    public void SetAnimatorTrigger(string triggerName)
    {
        animator.SetTrigger(triggerName);
    }

    private void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        GetComponent<RTS_Camera>().enabled = false;
    }
}